<?php
/**
 * @copyright NetMonsters <team@netmonsters.ru>
 * @link http://netmonsters.ru
 * @package Majestic
 * @subpackage Model
 * @since 2010-02-23
 */

class I18N
{
    
    static protected $domain  = 'default';
    static protected $locales = array();
    static protected $bidi    = array();
    static protected $langs   = array();
    static protected $default = 'ru';
    
    static protected $lang    = '';
    static protected $locale  = '';


    /**
     * @throws InitializationException
     * @internal mixed $lang default language set
     */
    static public function init()
    {
        $config = Config::get(__CLASS__);

        if (!is_array($config['locales'])) {
            throw new InitializationException('key \'locales\' array\'s config is empty');
        }

        self::$locales = $config['locales'];
        
        if (isset($config['bidi'])) {
            self::$bidi = $config['bidi'];
        }
        
        if (isset($config['default'])) {
            self::$default = $config['default'];
        }
        
        // language switching
        if ($lang = Env::Post('lang')) {
            self::setLang($lang);
            header('Location: ' . Env::getRequestUri());
            exit();
        }
        
        self::$lang = Env::Cookie('lang', self::getAcceptLanguage());
        self::setLang(self::$lang);
        self::$locale = self::$locales[self::$lang];
        self::initForLocale(self::$locale);
    }
    
    static public function initForLocale($locale)
    {
        putenv('LANG=' . $locale);
        setlocale(LC_ALL, $locale . '.UTF-8');
        bindtextdomain(self::$domain, PATH . '/' . APP . '/src/i18n/');
        textdomain(self::$domain);
        bind_textdomain_codeset(self::$domain, 'UTF-8');        
    }
    
    static protected function getAcceptLanguage()
    {
        $lang = self::$default;
        
        if ($accept = Env::Server('HTTP_ACCEPT_LANGUAGE')) {
            $accept = explode(',', $accept);
            foreach ($accept as $a) {
                if (($pos = strpos($a, ';q=')) !== false) {
                    $a = substr($a, 0, $pos);
                }
                if (($pos = strpos($a, '-')) !== false) {
                    $a = substr($a, 0, $pos) . '_' . strtoupper(substr($a, $pos + 1));
                }
                if (isset(self::$locales[$a])) {
                    $lang = $a;
                    break;
                }
                if ($key = array_search($a, self::$locales)) {
                    $lang = $key;
                    break;
                }
            }
        }
        return $lang;
    }
    
    static protected function setLang($lang)
    {
        if (!array_key_exists($lang, self::$locales)) {
            $lang = self::$default;
        }
        Env::setCookie('lang', $lang, time()+60*60*24*30, '/');
    }
    
    static public function getLang()
    {
        return self::$lang;
    }
    
    static public function getDefaultLang()
    {
        return self::$default;
    }
    
    static public function setLangs($langs = array())
    {
        self::$langs = $langs;
    }
    
    static public function getLangs()
    {
        return self::$langs;
    }
    
    static public function getLangName($code = false)
    {
        if (!$code) {
            $code = self::$lang;
        }
        if (isset(self::$langs[$code])) {
            return self::$langs[$code];
        } else {
            return false;
        }
    }
}