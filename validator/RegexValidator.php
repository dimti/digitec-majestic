<?php
/**
 * @copyright NetMonsters <team@netmonsters.ru>
 * @link http://netmonsters.ru
 * @package Majestic
 * @subpackage validator
 * @since 2010-04-26
 */

class RegexValidator extends Validator
{

    const NOT_MATCH = 'regex_not_match';
    
    protected $vars = array('regex');
    protected $templates = array(self::NOT_MATCH => '"%value%" does not match against regex "%regex%"');
    
    protected $regex;
    
    public function __construct($regex)
    {
        $this->regex = $regex;
    }
    
    public function isValid($value, $context = null)
    {
        $this->setValue($value);

        $status = preg_match($this->regex, $value);
        if ($status === false) {
            throw new GeneralException('Internal error matching regex "' . $this->regex . ' against value "' . $value . '"');
        }
        if (!$status) {
            $this->error();
            return false;
        }
        return true;
    }
}