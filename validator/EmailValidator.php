<?php
/**
 * @copyright NetMonsters <team@netmonsters.ru>
 * @link http://netmonsters.ru
 * @package Majestic
 * @subpackage validator
 * @since 2010-04-26
 */

class EmailValidator extends RegexValidator
{
    protected $regex = '/^([a-z0-9._-]{2,23})\@([a-z0-9-]{2,22}\.)+\w{2,5}$/i';
    
    public function __construct(){}
}