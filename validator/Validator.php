<?php
/**
 * @copyright NetMonsters <team@netmonsters.ru>
 * @link http://netmonsters.ru
 * @package Majestic
 * @subpackage validator
 * @since 2010-04-24
 */

abstract class Validator implements iValidator
{
    
    protected $value;
    protected $message;
    protected $vars = array();
    protected $templates = array();

    
    public function getMessage()
    {
        return $this->message;
    }
    
    protected function setValue($value)
    {
        $this->value = (string) $value;
        $this->message = null;
    }
    
    public function setMessage($message, $key = null)
    {
        if ($key === null) {
            $key = current(array_keys($this->templates));
        }
        $this->templates[$key] = $message;
    }
    
    protected function error($template = null, $value = null)
    {
        if ($template === null) {
            $template = current(array_keys($this->templates));
        }
        if ($value === null) {
            $value = $this->value;
        }
        $this->message = $this->createMessage($template, $value);
    }
    
    protected function createMessage($template, $value)
    {
        if (!isset($this->templates[$template])) {
            throw new GeneralException('Message template "' . $template . '" unknown.');
        }
        
        $message = $this->templates[$template];
        if (strpos($message, '%') !== false) {
            $message = str_replace('%value%', (string) $value, $message);
            foreach ($this->vars as $property) {
                if (property_exists($this, $property)) {
                    $message = str_replace("%$property%", (string) $this->$property, $message);
                }
            }
        }
        return $message;
    }
}