<?php
/**
 * @copyright NetMonsters <team@netmonsters.ru>
 * @link http://netmonsters.ru
 * @package Majestic
 * @subpackage validator
 * @since 2010-04-26
 */

class EqualValidator extends Validator
{

    const NOT_EQUAL = 'not_match';
    
    protected $templates = array(self::NOT_EQUAL => 'Tokens do not match');
    
    protected $token;
    
    public function __construct($token)
    {
        $this->token = $token;
    }
    
    public function isValid($value, $context = null)
    {
        $this->setValue($value);
        if ($this->token === null) {
            throw new InitializationException('Token not defined.');
        }

        if ($value !== $this->token)  {
            $this->error();
            return false;
        }
        return true;
    }
}