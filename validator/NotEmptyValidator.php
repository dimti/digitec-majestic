<?php
/**
 * @copyright NetMonsters <team@netmonsters.ru>
 * @link http://netmonsters.ru
 * @package Majestic
 * @subpackage validator
 * @since 2010-04-26
 */

class NotEmptyValidator extends Validator
{

    const IS_EMPTY = 'is_empty';
    
    protected $templates = array(self::IS_EMPTY => 'Value is required and can\'t be empty');
    
    public function isValid($value, $context = null)
    {
        $this->setValue($value);

        if (is_string($value) && $value === '') {
            $this->error();
            return false;
        } elseif (!is_string($value) && empty($value)) {
            $this->error();
            return false;
        }
        return true;
    }
}