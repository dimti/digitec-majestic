<?php
/**
 * @copyright NetMonsters <team@netmonsters.ru>
 * @link http://netmonsters.ru
 * @package Majestic
 * @subpackage validator
 * @since 2010-04-25
 */

interface iValidator
{
    public function isValid($value, $context = null);
    public function getMessage();
}