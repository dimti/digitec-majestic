<?php
/**
 * @copyright NetMonsters <team@netmonsters.ru>
 * @link http://netmonsters.ru
 * @package Majestic
 * @subpackage Model
 * @since 2010-02-17
 */

class Config extends Registry
{
    
    private static $_class_name = 'Config';
    
    static public function set($name, $value)
    {
        if (is_array($value)) {
            $value = new ConfigArray($value);
        }
        self::getInstance()->offsetSet($name, $value);
    }
}

class ConfigArray extends ArrayObject
{
    public function __construct($array)
    {
        parent::__construct($array, ArrayObject::ARRAY_AS_PROPS);
    }
    
    public function offsetGet($index) 
    {
        if (!$this->offsetExists($index)) {
            throw new GeneralException('Configuration variable "' . $index . '" undefined');
        }
        return parent::offsetGet($index);
    }
}