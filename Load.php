<?php
/**
 * @copyright NetMonsters <team@netmonsters.ru>
 * @link http://netmonsters.ru
 * @package Majestic
 * @subpackage Load
 * @since 2010-02-24
 */

class Load
{

    static protected $file;

    static protected $autoload;

    static protected $exclude = array();

    /**
     * Add exclude path for autoload. Should be called before setAutoloadFrom
     * @static
     * @param array $exclude list of relative path (from project root)
     * @return void
     */
    static public function setExclude($exclude = array())
    {
        if(!is_array($exclude)) {
            $exclude = array($exclude);
        }
        self::$exclude = array_merge(self::$exclude, $exclude);
    }
    
    static public function setAutoloadFrom($file)
    {
        self::$file = $file;
        if (!file_exists(self::$file)) {
            self::buildAutoload();
        }
        self::$autoload = require(self::$file);
        spl_autoload_register(array(
            __CLASS__,
            'autoload'));
    }

    static public function autoload($class)
    {
        if (isset(self::$autoload[$class])) {
            require(PATH . self::$autoload[$class]);
            return;
        }
        if (Config::get('DEBUG')) {
            if (!isset(self::$autoload[$class])) {
                self::buildAutoload();
            }
            if (isset(self::$autoload[$class])) {
                require(PATH . self::$autoload[$class]);
            }
        }
    }

    static public function getFilePath($class)
    {
        return self::$autoload[$class];
    }

    static protected function buildAutoload()
    {
        ignore_user_abort(true);
        $dir = dirname(self::$file);
        if (!file_exists($dir) && !mkdir($dir)) {
            trigger_error('Can\'t create directory: "' . $dir . '"', E_USER_ERROR);
        }

        $scan = array(PATH . '/' . APP . '/src', PATH . '/lib');
        $exclude = array_merge(self::$exclude,  array(PATH . '/.git', PATH . '/lib/core/tests', PATH . '/lib/core/.git'));

        require_once(PATH . '/lib/core/util/AutoloadBuilder.php');

        $builder = new AutoloadBuilder(self::$file, $scan, $exclude);
        $builder->build();
        ignore_user_abort(false);
    }
}