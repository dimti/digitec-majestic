<?php
/**
 * @copyright NetMonsters <team@netmonsters.ru>
 * @link http://netmonsters.ru
 * @package Majestic
 * @subpackage Cache
 * @since 2010-03-04
 */

class Cacher
{

    /**
     * Initialized cachers
     *
     * @var array
     */
    static protected $caches = array();

    /**
     * @param $cacher
     * @param null|string $config
     * @return Cache
     * @throws InitializationException
     */
    static public function get($cacher, $config = null)
    {
        if (!isset(self::$caches[$cacher])) {
            if (!$config) {
                $config = Config::get($cacher);
            }
            $cache = new $cacher($config);
            if (!$cache instanceof Cache) {
                throw new InitializationException('Cache driver "' . $cacher . '" must extends Cache');
            }
            self::$caches[$cacher] = $cache;
        }
        return self::$caches[$cacher];
    }
}