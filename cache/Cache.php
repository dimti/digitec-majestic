<?php
/**
 * @copyright NetMonsters <team@netmonsters.ru>
 * @link http://netmonsters.ru
 * @package Majestic
 * @subpackage Cache
 * @since 2010-03-04
 */

abstract class Cache
{

    /**
     * Add an item to the cache
     *
     * @param string $key
     * @param mixed $value
     * @param int $expire
     * @return bool
     */
    abstract public function add($key, $value, $expire = 0);

    /**
     * Decrement item's value
     *
     * @param string $key
     * @param int $decrement
     * @return bool
     */
    abstract public function decrement($key, $decrement = 1);

    /**
     * Delete item from the cache
     *
     * @param string $key
     * @return bool
     */
    abstract public function del($key);

    /**
     * Flush all existing items
     *
     * @return bool
     */
    abstract public function flush();

    /**
     * Retrieve item from the cache
     *
     * @param mixed $key
     * @return mixed
     */
    abstract public function get($key);

    /**
     * Increment item's value
     *
     * @param string $key
     * @param int $increment
     * @return bool
     */
    abstract public function increment($key, $increment = 1);

    /**
     * Replace value of the existing item
     *
     * @param string $key
     * @param mixed $value
     * @param int $expire
     * @return bool
     */
    abstract public function replace($key, $value, $expire = 0);

    /**
     * Store data in the cache
     *
     * @param string $key
     * @param mixed $value
     * @param int $expire
     * @return bool
     */
    abstract public function set($key, $value, $expire = 0);
}