<?php
/**
 * @copyright NetMonsters <team@netmonsters.ru>
 * @link http://netmonsters.ru
 * @package Majestic
 * @subpackage Cache
 * @since 2010-03-10
 */

class CacheKey
{

    /**
     * @var Cache
     */
    protected $cacher;

    /**
     * @var CacheKey
     */
    protected $key;

    protected $expire = 0;

    /**
     * @param Cacher $cacher
     * @param string $key
     * @param mixed $params
     * @param int $expire
     * @return CacheKey
     */
    public function __construct($cacher, $key, $params = array(), $expire = 0)
    {
        $this->cacher = $cacher;
        $this->key = $key;
        if ($params) {
            $params = (is_array($params)) ? implode('|', $params) : $params;
            $this->key = $key . '_' . $params;
        }
        $this->expire = $expire;
    }

    protected function getExpire()
    {
        return $this->expire;
    }

    /**
     * @param int $expire
     */
    public function setExpire($expire)
    {
        $this->expire = $expire;
    }

    public function get()
    {
        return $this->cacher->get($this->key);
    }

    /**
     * @param mixed $value
     * @return bool
     */
    public function set($value)
    {
        return $this->cacher->set($this->key, $value, $this->expire);
    }

    public function del()
    {
        return $this->cacher->del($this->key);
    }
}