<?php
/**
 * @copyright NetMonsters <team@netmonsters.ru>
 * @link http://netmonsters.ru
 * @package Majestic
 * @subpackage db
 * @since 2010-02-16
 */

abstract class DbDriver
{
    
    /**
     * Database connection
     * 
     * @var object
     */
    protected $connection = null;
    
    /**
     * Configuration data
     * 
     * @var array
     */
    protected $config = array();
    
    
    public function __construct($config)
    {
        $this->checkConfig($config);
        $this->config = $config;
    }
    
    protected function checkConfig($config)
    {
        $required = array('database', 'username', 'password', 'hostname');
        foreach ($required as $option) {
            if (!isset($config[$option])) {
                throw new GeneralException('Configuration must have a "' . $option . '".');
            }
        }
    }

    public function getConnection()
    {
        $this->connect();
        return $this->connection;
    }

    /**
     * @param mixed $request
     * @param mixed $params
     * @return DbStatement
     */
    public function query($request, $params = array())
    {
        $this->connect();
        if (!is_array($params)) {
            $params = array($params);
        }
        $stmt = $this->prepare($request);
        $stmt->execute($params);
        return $stmt;
    }
    

    /* Abstract methods  */

    abstract public function insert($table, $data);

    abstract public function update($table, $data, $condition);
    
    abstract public function delete($table, $condition);

    abstract public function prepare($request);
    
    abstract public function getInsertId($table = null, $key = null);
    
    abstract public function isConnected();
    
    abstract public function disconnect();
    
    abstract protected function connect();
}