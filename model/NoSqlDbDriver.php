<?php
/**
 * @copyright NetMonsters <team@netmonsters.ru>
 * @link http://netmonsters.ru
 * @package Majestic
 * @subpackage db
 * @since 2011-11-11
 */

abstract class NoSqlDbDriver extends DbDriver
{
    
    abstract function find($collection, $condition = array(), $fields = array());
}