<?php
/**
 * @copyright NetMonsters <team@netmonsters.ru>
 * @link http://netmonsters.ru
 * @package Majestic
 * @subpackage db
 * @since 2010-02-16
 */

class Db
{
    
    const FETCH_ASSOC = 2;
    const FETCH_NUM   = 3;
    const FETCH_BOTH  = 4;
    const FETCH_OBJ   = 5;

    /**
     * Databases connections
     * 
     * @var array
     */
    static protected $connections = array();
    
    /**
     * Connect to database
     * 
     * @param string $name Database name. If not set 'default' will be used.
     * @param array $config Configuration array.
     *                       
     * @return DbDriver
     * @throws InitializationException
     */
    static public function connect($name = 'default', $config = null)
    {
        if (!isset(self::$connections[$name])) {
            if (!$config) {
                if (!is_object(Config::get(__CLASS__))) {
                     throw new InitializationException('Trying to get property of non-object');
                }
                $config = Config::get(__CLASS__)->$name;
            }
                
            if (!is_array($config)) {
                throw new InitializationException('Connection parameters must be an array');
            }
            
            $driver = 'MySQLiDriver';
            if (isset($config['driver'])) {
                $driver = $config['driver'];
                unset($config['driver']);
            }
            
            $connection = new $driver($config);
            
            if (!$connection instanceof DbDriver) {
                throw new InitializationException('Database driver must extends DbDriver');
            }
            self::$connections[$name] = $connection;
        }
        return self::$connections[$name];
    }
}