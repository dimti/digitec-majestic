<?php
/**
 * @copyright NetMonsters <team@netmonsters.ru>
 * @link http://netmonsters.ru
 * @package Majestic
 * @subpackage db
 * @since 2011-11-10
 */

/**
 * @property Mongo $connection
 */
class MongoDriver extends NoSqlDbDriver
{
    protected $last_insert_id = 0;

    protected $db_name = 'admin';

    protected $db = null;

    protected function getCollection($name)
    {
        if (!$this->connection) {
            $this->connect();
        }
        return $this->connection->selectCollection($this->db, $name);
    }

    public function count($collection, $query = array(), $limit = 0, $skip = 0)
    {
        $command = MongoCommandBuilder::factory(MongoCommandBuilder::COUNT, $this->getCollection($collection));
        $params = array(
            'condition' => $query,
            'limit' => $limit,
            'skip' => $skip
        );

        return $this->query($command, $params)->affectedRows();
    }

    /**
     * @param string $collection Mongo collection name to search in
     * @param array $condition Search conditions
     * @param array $fields Fields to return in result set
     * @return MongoStatement
     */
    public function find($collection, $condition = array(), $fields = array())
    {
        $command = MongoCommandBuilder::factory(MongoCommandBuilder::FIND, $this->getCollection($collection));
        $params = array(
            'condition' => $condition,
            'fields' => $fields
        );

        return $this->query($command, $params);
    }

    public function findAndModify($collection, $query, $update, $sort = array(), $field = array(), $upsert = false, $new = false, $remove = false)
    {
        $command = MongoCommandBuilder::factory(MongoCommandBuilder::COMMAND, $this->getCollection($collection));
        $cmd = array(
            'findAndModify' => $collection,
            'query' => $query,
            'update' => $update,
            'sort' => $sort,
            'fields' => $field,
            'new' => $new,
            'upsert' => $upsert,
            'remove' => $remove
        );
        $params = array('command' => $cmd);
        return $this->query($command, $params);
    }

    public function command($collection, $cmd)
    {
        $command = MongoCommandBuilder::factory(MongoCommandBuilder::COMMAND, $this->getCollection($collection));
        $params = array('command' => $cmd);
        return $this->query($command, $params);
    }

    public function insert($collection, $data, $multiple = false, $safe = true)
    {
        $command = MongoCommandBuilder::factory(MongoCommandBuilder::INSERT, $this->getCollection($collection));
        $params = array(
            'data' => $data,
            'safe' => $safe,
            'multiple' => $multiple
        );
        $result = $this->query($command, $params);
        $this->last_insert_id = $result->getInsertId();
        return $result->affectedRows();
    }

    public function update($collection, $data, $condition = array(), $multiple = true, $upsert = false, $safe = true)
    {
        $command = MongoCommandBuilder::factory(MongoCommandBuilder::UPDATE, $this->getCollection($collection));
        $params = array(
            'data' => $data,
            'condition' => $condition,
            'multiple' => $multiple,
            'upsert' => $upsert,
            'safe' => $safe
        );

        return $this->query($command, $params)->affectedRows();
    }

    public function delete($collection, $condition = array(), $safe = true)
    {
        $command = MongoCommandBuilder::factory(MongoCommandBuilder::REMOVE, $this->getCollection($collection));
        $params = array(
            'condition' => $condition,
            'safe' => $safe
        );

        return $this->query($command, $params)->affectedRows();
    }


    /**
     * @param mixed $request
     * @return MongoStatement
     */
    public function prepare($request)
    {
        return new MongoStatement($this, $request);
    }

    public function getInsertId($table = null, $key = null)
    {
        return $this->last_insert_id;
    }

    public function isConnected()
    {
        if (!is_null($this->connection)) {
            return $this->connection->connected;
        } else {
            return false;
        }
    }

    public function disconnect()
    {
        if ($this->isConnected()) {
            $this->connection->close();
        }
        $this->connection = null;
    }

    protected function connect()
    {
        if ($this->connection) {
            return;
        }

        $host = $this->config['hostname'];
        $port = isset($this->config['port']) ? ':' . (string) $this->config['port'] : '';

        $this->config = array(
            'username' => $this->config['username'],
            'password' => $this->config['password'],
            'db' => $this->config['database']
        );

        $this->connection = new Mongo('mongodb://' . $host . $port, $this->config);
        $this->db = $this->connection->selectDB($this->config['db']);
    }
}
