<?php

/**
 * Класс модели данных
 *
 * @copyright NetMonsters <team@netmonsters.ru>
 * @link http://netmonsters.ru
 * @package Majestic
 * @subpackage Model
 * @since 2010-02-16
 */

abstract class Model
{

    /**
     * DbDriver instance
     *
     * @var DbDriver
     */
    protected $db;

    /**
     * Cache instance
     *
     * @var Cache
     */
    protected $cache;
    
    /**
     * Custom expiration time for keys
     * 
     * @var mixed
     */
    protected $cache_keys  = array();
    
    /**
     * Caches to clean.
     * 
     * @var mixed
     */
    protected $caches_clean = array();

    protected $table;

    protected $key          = 'id';
    

    public function __construct($connection = 'default')
    {
        $this->db = Db::connect($connection);
    }

    /**
     * @return int
     */
    public function getInsertId()
    {
        return $this->db->getInsertId($this->table(), $this->key);
    }

    /**
     * @param array $data
     * @return int Id of inserted row
     */
    public function insert($data)
    {
        $affected = $this->db->insert($this->table(), $data);
        return ($this->getInsertId()) ? $this->getInsertId() : $affected;
    }

    /**
     * @param array $data
     * @param mixed $where
     * @return int Number of affected rows
     */
    public function update($data, $where)
    {
        return $this->db->update($this->table(), $data, $where);
    }
    
    /**
     * @return string
     */
    protected function table()
    {
        if (!$this->table) {
            $this->table = substr(strtolower(get_class($this)), 0, -5/*strlen('Model')*/);
        }
        return $this->table;
    }

    
    /* Cache workaround */
    
    /**
     * @return Cache
     */
    public function getCache()
    {
        if (!$this->cache) {
            $this->cache = Cacher::get(Config::get(__CLASS__, 'MemcacheCache'));
        }
        return $this->cache;
    }
    
    /**
     * @param string $name
     * @param array $params
     * @return CacheKey
     */
    protected function cacheKey($name, $params = array())
    {
        $expire = (isset($this->cache_keys[$name])) ? ($this->cache_keys[$name] * 60) : 0;
        return new CacheKey($this->getCache(), $name, $params, $expire);
    }
    
    /**
     * @param CacheKey $cache
     */
    protected function addCleanCache($cache)
    {
        $this->caches_clean[] = $cache;
    }
    
    protected function cleanCaches()
    {
        // cleaning caches
        foreach ($this->caches_clean as $cache) {
            $cache->del();
        }
        $this->caches_clean = array();
    }

    /**
     * Abstract methods
     */

    /**
     * @param int $id
     * @return object
     */
    abstract public function get($id);

    /**
     * @param int $id Int id
     * @return int Number of affected rows
     */
    abstract public function delete($id);

    /**
     * @param string $data Request
     * @param array $params Request parameters
     * @param string $field Requested field name
     * @param CacheKey $cache_key Key for caching in
     */
    abstract protected function fetchField($data, $params = array(), $field, $cache_key = null);

    /**
     * @param string $data Request
     * @param array $params Request parameters
     * @param CacheKey $cache_key Key for caching in
     */
    abstract  protected function fetch($data, $params = array(), $cache_key = null);

    /**
     * @param string $data
     * @param array $params
     * @param CacheKey $cache_key
     */
    abstract protected function fetchAll($data, $params = array(), $cache_key = null);
}