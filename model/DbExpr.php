<?php
/**
 * @copyright NetMonsters <team@netmonsters.ru>
 * @link http://netmonsters.ru
 * @package Majestic
 * @subpackage db
 * @since 2010-02-19
 */

class DbExpr
{
    
    protected $expression;
    
    /**
     * @param string $expression
     * @return DbExpr
     */
    public function __construct($expression)
    {
        $this->expression = (string) $expression;
    }
    
    public function __toString()
    {
        return $this->expression;
    }
}