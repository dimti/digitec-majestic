<?php
/**
 * @copyright NetMonsters <team@netmonsters.ru>
 * @link http://netmonsters.ru
 * @package Majestic
 * @subpackage validator
 * @since 2010-04-26
 */

class CaptchaValidator extends Validator
{

    const WRONG_CODE = 'is_empty';

    protected $templates = array(self::WRONG_CODE => 'Entered code wrong');

    public function isValid($value, $context = null)
    {
        if (!$context || !isset($context['ctoken']) || !isset($context['ccode'])) {
            $this->error();
            return false;
        }
        $captcha = new Captcha();
        if ($captcha->checkCode($context['ctoken'], $context['ccode'])) {
            return true;
        }
        $this->error();
        return false;
    }
}