<?php
/**
 * @copyright NetMonsters <team@netmonsters.ru>
 * @link http://netmonsters.ru
 * @package Majestic
 * @subpackage captcha
 * @since 2010-04-24
 */

class CaptchaImageAction
{

    public function __construct()
    {
        $captcha = new Captcha();
        echo $captcha->getImage(Env::Get('ctoken'));
        exit();
    }
}