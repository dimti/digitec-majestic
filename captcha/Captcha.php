<?php
/**
 * @copyright NetMonsters <team@netmonsters.ru>
 * @link http://netmonsters.ru
 * @package Majestic
 * @subpackage captcha
 * @since 2010-04-24
 */

class Captcha
{

    protected $font_size = 38;

    protected $width = 200;

    protected $height = 70;


    public function getImage($token)
    {
        $font = dirname(__FILE__) . '/poh.ttf';

        $image = imagecreatetruecolor($this->width, $this->height);
        // белый фон
        $background = imagecolorallocate($image, 255, 255, 255);
        imagefilledrectangle($image, 0, 0, $this->width, $this->height, $background);

        // основная магия тут
        if (Session::get('_ctoken') == $token && Session::get('_ccode')) {

            $code = Session::get('_ccode');

            $color = imagecolorallocate($image, 81, 81, 81);

            for ($j = 0; $j < 5; $j++) {
                imageellipse($image, rand(-$this->width, $this->width * 2),
                             rand(-$this->height, $this->height * 2),
                             $this->width, $this->width, $color);
            }

            $letters = preg_split("//u", strtoupper($code));
            $length = count($letters);
            $step = floor($this->width / $length);
            $i = 2;

            foreach ($letters as $key => $letter) {
                imagettftext($image, $this->font_size, 0,
                             rand($i + 2, $i + 4), ($this->font_size + $this->height) / 2,
                             $color, $font, $letter);
                $i = $i + $step;
            }
        }

        ob_start();
        header("Cache-Control: no-store, no-cache, must-revalidate");
        header("Content-Type: image/jpg");
        imagejpeg($image, '', 100);
        imagedestroy($image);
        return ob_get_clean();
    }

    public function getToken()
    {
        $token = md5(microtime() . uniqid());
        $code = strtoupper(substr(str_ireplace(array('0', 'O'), '', md5(time() . 'captcha' . rand(0, 1000))), 1, 5));
        Session::set('_ctoken', $token);
        Session::set('_ccode', $code);
        return $token;
    }

    public function checkCode($token, $code)
    {
        if (Session::get('_ctoken') == $token && Session::get('_ccode') == strtoupper($code)) {
            // Housekeeping
            Session::del('_ccode');
            Session::del('_ctoken');
            return true;
        }
        return false;
    }
}