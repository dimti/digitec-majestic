<?php

/*
 * @copyright NetMonsters <team@netmonsters.ru>
 * @link http://netmonsters.ru
 * @package Majestic
 * @subpackage UnitTests
 * @since 2011-12-02
 *
 * Test set for Redis
 */

require_once 'redis/RedisDebugTest.php';
require_once 'redis/RedisManagerTest.php';

class PackageRedisTests
{
    public static function suite()
    {
        $suite = new PHPUnit_Framework_TestSuite('Redis');

        $suite->addTestSuite('RedisDebugTest');
        $suite->addTestSuite('RedisManagerTest');

        return $suite;
    }
}