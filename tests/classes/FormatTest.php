<?php

/*
 * @copyright NetMonsters <team@netmonsters.ru>
 * @link http://netmonsters.ru
 * @package Majestic
 * @subpackage UnitTests
 * @since 2011-10-26
 *
 * Unit tests for Format class
 */

require_once dirname(__FILE__) . '/../../classes/Format.class.php';
class FormatTest extends PHPUnit_Framework_TestCase
{

    public function setUp()
    {
    }

    public function testCurrency()
    {
        $this->assertSame('руб.', Format::getCurrency());
        Format::setCurrencySymbol('usd');
        $this->assertSame('usd', Format::getCurrency());
    }

    /**
     * @TODO: ???WHY??? itn2Money - utf non-breaking space + &nbsp;
     */
    public function testInt2Money()
    {
        $a = '2 000&nbsp;руб.';
        $b = Format::int2money(200000, true, false);

        //$this->printStr($a);
        //$this->printStr($b);
        
        $this->assertSame('2' . chr(0xC2) . chr(0xA0) . '000&nbsp;руб.', Format::int2money(200000, true, false));
        $this->assertSame('20 000,00', Format::int2money(2000000, false));
        Format::setCurrencySymbol('usd');
        $this->assertSame('200,00&nbsp;usd', Format::int2money(20000, true));
    }

    public function testMoney2Int()
    {
        $this->assertSame(20000, Format::money2int('200,00', false));
        $this->assertSame(20000, Format::money2int('200', false));
        $this->assertSame(2000, Format::money2int('2 000руб.'));
    }

    public function testInt2Time()
    {
        $time = 14 * 60 * 60 + 44 * 60 + 24;

        $this->assertSame('14:44:24', Format::int2time($time));
        $this->assertSame('00:00:00', Format::int2time());
    }

    public function testInt2Date()
    {
        $this->assertSame(date('H:i d.m.Y', 0), Format::int2date());
        $this->assertSame(date('H:i d.m.Y'), Format::int2date(time()));
        $this->assertSame(date('d.m.Y'), Format::int2date(time(), false));
        $this->assertSame('20.04.2011', Format::int2date(strtotime('20 April 2011'), false));
        $this->assertSame('21:10 20.04.2011', Format::int2date(strtotime('21:10:30 20 April 2011')));
    }

    public function testInt2rusDate()
    {
        $this->assertSame('10 января 1990', Format::int2rusDate(strtotime('10 January 1990')));
        $this->assertSame('19:10 10 января 1990', Format::int2rusDate(strtotime('19:10:59 10 January 1990'), true));
    }

    public function testSetTimezoneOffset()
    {
        Format::setTimezoneOffset(3);
        $class = new ReflectionClass('Format');
        $offset = $class->getProperty('timezone_offset');
        $offset->setAccessible(true);
        $this->assertSame(3, $offset->getValue());
    }

    public function testSetDateTimeFormat()
    {
        Format::setDateFormat('H_i d::m::Y', 'd--m--Y');
        $this->assertSame('14_22 20::04::2011', Format::int2date(strtotime('14:22:00 20 April 2011')));
        $this->assertSame('20--04--2011', Format::int2date(strtotime('14:22:00 20 April 2011'), false));
    }

    public function testSetTodayFormat()
    {
        Format::setTodayFormat('H_i d::m::Y', 'd--m--Y');
        $this->assertSame(date('H_i d::m::Y', strtotime('+2hours' )), Format::int2date(strtotime('+2 hours')));
        $this->assertSame(date('d--m--Y', strtotime('+2hours' )), Format::int2date(strtotime('+2 hours'), false));

    }

    public function testTime2Int()
    {
        $time = 14 * 60 * 60 + 44 * 60 + 24;
        $this->assertSame($time, Format::time2int('14:44:24'));
        $this->assertSame(14, Format::time2int('14:44'));
        $this->assertSame(14, Format::time2int('14:44:32:53:12'));
    }

    public function testDate2Int()
    {
        $this->assertSame(strtotime('2 Jan 2010'), Format::date2int('2 Jan 2010'));
    }

    public function testInt2Phone()
    {
        $this->assertSame('777-77-77', Format::int2phone(7777777));
        $this->assertSame('(123) 456-78-90', Format::int2phone(1234567890));
        $this->assertSame('', Format::int2phone(12890));
        $this->assertSame('', Format::int2phone('asdas'));
    }

    /**
     * @TODO: change name, check values(only Moscow???)
     */
    public function testPhone2Int()
    {
        $this->assertSame('4951234567', Format::phone2int('123-45-67'));
        $this->assertSame('9261234567', Format::phone2int('926-123-45-67'));
        $this->assertSame('', Format::phone2int('8-926-123-45-67'));
        $this->assertSame('', Format::phone2int('12-45-67'));
        $this->assertSame('', Format::phone2int('not a phone'));
    }

    public function testBytes2MB()
    {
        $this->assertSame('1МБ', Format::bytes2MB(1048576));
    }

    public function testBytes2KB()
    {
        $this->assertSame('2КБ', Format::bytes2KB(2048));

    }


    public function tearDown()
    {
        Format::setCurrencySymbol('руб.');
        Format::setTimezoneOffset(0);
        Format::setDateFormat('H:i d.m.Y', 'd.m.Y');
        Format::setTodayFormat('H:i d.m.Y', 'd.m.Y');
        
    }

    private function printStr($a)
    {
        echo PHP_EOL;
        for($i = 0; $i < strlen($a); $i++) {
            echo sprintf(' 0x%X ', ord($a[$i]));
        }
        echo PHP_EOL;

    }

}
