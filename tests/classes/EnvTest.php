<?php

/*
 * @copyright NetMonsters <team@netmonsters.ru>
 * @link http://netmonsters.ru
 * @package Majestic
 * @subpackage UnitTests
 * @since 2011-10-26
 *
 * Unit tests for Env class
 */

require_once dirname(__FILE__) . '/../../exception/ErrorHandler.php';
require_once dirname(__FILE__) . '/../../app/router/Router.php';
require_once dirname(__FILE__) . '/../../app/FrontController.php';
require_once dirname(__FILE__) . '/../../classes/Env.class.php';

    
class EnvTest extends PHPUnit_Framework_TestCase
{

    /**
     * @runInSeparateProcess
     */
    public function testGetRequestUri()
    {

        Config::set('DEBUG', false);
        $_SERVER['REQUEST_URI'] = '/test/index.php?id=1&test=wet&id_theme=512';
        $this->assertSame('/test/index.php', Env::getRequestUri());
        $_SERVER['REQUEST_URI'] = '/tes?t/index.php?id=1&test=wet&id_theme=512';
        $this->assertSame('/test/index.php', Env::getRequestUri());
    }

    /**
     * @runInSeparateProcess
     */
    public function testTrimBaseRequestUri()
    {

        Config::set('DEBUG', false);
        $class = new ReflectionClass('Env');
        $this->started = $class->getProperty('request');
        $this->started->setAccessible(true);
        $this->started->setValue(null, array());

        FrontController::getInstance()->setBaseUrl('/test');
        $_SERVER['REQUEST_URI'] = '/test/index.php?id=1&test=wet&id_theme=512';
        $this->assertSame('/index.php', Env::getRequestUri(true));
    }
    
    public function testServer()
    {
        $this->assertSame($_SERVER, Env::Server());
        $this->assertSame($_SERVER['DOCUMENT_ROOT'], Env::Server('DOCUMENT_ROOT'));
        $this->assertNotEmpty(Env::Server());
        $this->assertArrayHasKey('DOCUMENT_ROOT', Env::Server());
    }

    public function testCookie()
    {
        $this->assertTrue(Env::setCookie('var', 'value', 20));
        $_COOKIE['var'] = 'value';
        $this->assertSame(array('var' => 'value'), Env::Cookie());
        $this->assertSame('value', Env::Cookie('var'));
        $this->assertSame('default', Env::Cookie('new', 'default'));
    }

    public function testPost()
    {
        $_POST['var'] = 'value';
        $this->assertSame(array('var' => 'value'), Env::Post());
        $this->assertSame('value', Env::Post('var'));
        $this->assertSame('default', Env::Post('new', 'default'));
    }

    public function testGet()
    {
        $_GET['var'] = 'value';
        $this->assertSame(array('var' => 'value'), Env::Get());
        $this->assertSame('value', Env::Get('var'));
        $this->assertSame('default', Env::Get('new', 'default'));
    }

    public function testFiles()
    {
        $this->assertSame('default', Env::Files('file.txt', 'default'));
        $this->assertSame(array(), Env::Files());
    }

    public function testUnsetFiles()
    {
        unset($_FILES);
        $this->assertSame('default', Env::Files('file.txt', 'default'));
        $_FILES['file'] = array('name' => 'files', 'path' => '/');
        $this->assertSame(array('name' => 'files', 'path' => '/'), Env::Files('file', 'default'));
        $this->assertSame('files', Env::Files('file', 'empty', 'name'));
    }

    public function testParams()
    {
        Env::setParams(array('name' => 'tony', 'age' => 21));
        $this->assertSame(array('name' => 'tony', 'age' => 21), Env::getParam());
        Env::setParams(array('sex' => 'male'));
        $this->assertSame(array('name' => 'tony', 'age' => 21, 'sex' => 'male'), Env::getParam());
        $this->assertSame('tony', Env::getParam('name'));
        $this->assertSame('default', Env::getParam('height', 'default'));
        Env::setParam('surname', 'grebnev');
        $this->assertSame('grebnev', Env::getParam('surname'));
    }

    public function tearDown()
    {
        $env = new ReflectionClass('Env');
        $params = $env->getProperty('params');
        $params->setAccessible(true);
        $params->setValue('Env', array());
    }

}
