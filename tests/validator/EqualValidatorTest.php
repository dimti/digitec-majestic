<?php

/*
 * @copyright NetMonsters <team@netmonsters.ru>
 * @link http://netmonsters.ru
 * @package Majestic
 * @subpackage UnitTests
 * @since 2011-10-07
 * 
 * Unit tests for RegexValdator class
 */

require_once dirname(__FILE__) . '/../../validator/iValidator.php';
require_once dirname(__FILE__) . '/../../validator/Validator.php';
require_once dirname(__FILE__) . '/../../validator/EqualValidator.php';
require_once dirname(__FILE__) . '/../../exception/InitializationException.php';

class EqualValidatorTest extends PHPUnit_Framework_TestCase
{
    public function testConstructor()
    {
        $validator = new EqualValidator('token');
        $this->assertFalse($validator->isValid('not token'));
        $this->assertTrue($validator->isValid('token'));
    }
    
    public function testNullToken()
    {
        $validator = new EqualValidator(null);
        $this->setExpectedException('InitializationException','Token not defined');
        $validator->isValid('not token');        
    }

}
   