<?php

/*
 * @copyright NetMonsters <team@netmonsters.ru>
 * @link http://netmonsters.ru
 * @package Majestic
 * @subpackage UnitTests
 * @since 2011-10-07
 * 
 * Unit tests for RegexValdator class
 */

require_once dirname(__FILE__) . '/../../validator/iValidator.php';
require_once dirname(__FILE__) . '/../../validator/Validator.php';
require_once dirname(__FILE__) . '/../../validator/RegexValidator.php';
require_once dirname(__FILE__) . '/../../validator/EmailValidator.php';

class EmailValidatorTest extends PHPUnit_Framework_TestCase
{

    public function testConstruct()
    {
        $validator = new EmailValidator();
        $this->assertTrue($validator->isValid('mail@mail.erw'));
        $this->assertFalse($validator->isValid('asdasd'));
    }
}