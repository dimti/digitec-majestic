<?php

/*
 * @copyright NetMonsters <team@netmonsters.ru>
 * @link http://netmonsters.ru
 * @package Majestic
 * @subpackage UnitTests
 * @since 2011-10-07
 * 
 * Unit tests for RegexValdator class
 */

require_once dirname(__FILE__) . '/../../validator/iValidator.php';
require_once dirname(__FILE__) . '/../../validator/Validator.php';
require_once dirname(__FILE__) . '/../../validator/NotEmptyValidator.php';

class NotEmptyValidatorTest extends PHPUnit_Framework_TestCase
{
    public function testValidator()
    {
        $validator = new NotEmptyValidator();
        $this->assertFalse($validator->isValid(''));
        $this->assertTrue($validator->isValid('token'));
        $this->assertTrue($validator->isValid(1212));
        $this->assertTrue($validator->isValid(array(1,2,2)));
        $this->assertFalse($validator->isValid(array()));
        $this->assertNotEmpty($validator->getMessage());
    }
    
    public function testEmptyValue()
    {
        $validator = new NotEmptyValidator(null);
        $this->assertFalse($validator->isValid(null));        
    }

}
   