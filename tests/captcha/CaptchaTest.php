<?php

/*
 * @copyright NetMonsters <team@netmonsters.ru>
 * @link http://netmonsters.ru
 * @package Majestic
 * @subpackage UnitTests
 * @since 2011-10-27
 * 
 * Unit tests for Captcha class
 */

require_once dirname(__FILE__) . '/../../session/Session.php';
require_once dirname(__FILE__) . '/../../captcha/Captcha.php';

class CaptchaTest extends PHPUnit_Framework_TestCase
{

    private $captcha;

    protected $width = 200;

    protected $height = 70;


    public function setUp()
    {
        $this->captcha = new Captcha();
    }

    /**
     * @TODO: give a path to custom font as a param
     */
    public function testGetImage()
    {
        $token = $this->captcha->getToken();
        $image = $this->captcha->getImage($token);
        $this->assertNotNull($image);

        $my_image = $this->createBlankImage();
        $this->assertNotSame($image, $my_image);
    }

    public function testGetImageWrongToken()
    {
        $token = $this->captcha->getToken();
        $image = $this->captcha->getImage('tony');
        $this->assertNotNull($image);

        $my_image = $this->createBlankImage();
        $this->assertSame($image, $my_image);
    }

    public function testGetImageEmptyCode()
    {
        $token = $this->captcha->getToken();
        Session::set('_ccode', '');
        $image = $this->captcha->getImage($token);
        $this->assertNotNull($image);

        $my_image = $this->createBlankImage();
        $this->assertSame($image, $my_image);
    }

    public function testGetToken()
    {
        $token = $this->captcha->getToken();
        $code = Session::get('_ccode');
        $this->assertNotEmpty($token);
        $this->assertNotEmpty($code);
        $this->assertSame(5, strlen($code));
        $this->assertSame(Session::get('_ctoken'), $token);
        $this->assertSame(32, strlen($token));
    }

    public function testCheckCode()
    {
        $token = Session::get('_ctoken');
        $code = Session::get('_ccode');
        $this->assertFalse($this->captcha->checkCode($token . 'asd', $code));
        $this->assertSame(Session::get('_ctoken'), $token);
        $this->assertTrue($this->captcha->checkCode($token, $code));
        $this->assertNull(Session::get('_ctoken'));
    }

    public function tearDown()
    {
        $sid = session_id();
        if (!empty($sid)) {
            Session::destroy();
        }
    }

    private function createBlankImage()
    {
        $image = imagecreatetruecolor($this->width, $this->height);
        $background = imagecolorallocate($image, 255, 255, 255);
        imagefilledrectangle($image, 0, 0, $this->width, $this->height, $background);
        ob_start();
        imagejpeg($image, '', 100);
        return ob_get_clean();
    }
}
    