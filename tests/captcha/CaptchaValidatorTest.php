<?php

/*
 * @copyright NetMonsters <team@netmonsters.ru>
 * @link http://netmonsters.ru
 * @package Majestic
 * @subpackage UnitTests
 * @since 2011-10-27
 * 
 * Unit tests for CaptchaValidator class
 */

require_once dirname(__FILE__) . '/../../session/Session.php';
require_once dirname(__FILE__) . '/../../validator/iValidator.php';
require_once dirname(__FILE__) . '/../../validator/Validator.php';
require_once dirname(__FILE__) . '/../../captcha/CaptchaValidator.php';
require_once dirname(__FILE__) . '/../../captcha/Captcha.php';

class CaptchaValidatorTest extends PHPUnit_Framework_TestCase
{

    /**
     * @TODO: CaptchaValidator->isValid() - $value param not used
     */
    public function testIsValid()
    {
        $captcha = new Captcha();
        $token = $captcha->getToken();
        $code = Session::get('_ccode');

        $validator = new CaptchaValidator();
        $this->assertTrue($validator->isValid(null, array('ctoken' => $token, 'ccode' => $code)));
        $this->assertFalse($validator->isValid(null, array('ctoken' => $token . 'asd', 'ccode' => $code)));
        $this->assertSame('Entered code wrong', $validator->getMessage());
    }

    public function testIsValidInvalid()
    {
        $validator = new CaptchaValidator();
        $this->assertFalse($validator->isValid(null, array()));
        $this->assertSame('Entered code wrong', $validator->getMessage());
    }
}