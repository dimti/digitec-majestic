<?php

/*
 * @copyright NetMonsters <team@netmonsters.ru>
 * @link http://netmonsters.ru
 * @package Majestic
 * @subpackage UnitTests
 * @since 2011-10-14
 * 
 * Unit tests for CacheKey class
 */


/**
 * @TODO: CacheKey->getExpire() method never used
 */

require_once dirname(__FILE__) . '/../../cache/Cache.php';
require_once dirname(__FILE__) . '/../../cache/CacheKey.php';

class CacheKeyTest extends PHPUnit_Framework_TestCase
{

    private $cache;
    
    
    public function testConstructor()
    {
        $mock = $this->getMock('Cache');
        
        $cache = new CacheKey($mock, 'anything', array('one', 'two', 'three'),200);
        $this->assertAttributeEquals('anything_one|two|three', 'key', $cache);
        
    }
    
    public function testExpire()
    {
        $this->cache = new CacheKey($this->getMock('Cache'), 'key');
        $this->assertAttributeEquals(0, 'expire', $this->cache);
        $this->cache->setExpire(20);
        $this->assertAttributeEquals(20, 'expire', $this->cache);
    }
    
    public function testGetExpire()
    {
        $this->cache = new CacheKey(null, 'key');
        $this->cache->setExpire(100);
        
        $getExpireMethod = new ReflectionMethod('CacheKey',  'getExpire');
        $getExpireMethod->setAccessible(TRUE);
        
        $this->assertSame(100, $getExpireMethod->invoke($this->cache));
    }
    
    public function testGetSet()
    {
        $mockCacher = $this->getMock('Cache');
        $mockCacher->expects($this->any())
                ->method('set')
                ->with('any', 'some', 0);

        $mockCacher->expects($this->any())
                ->method('get')
                ->with('any')
                ->will($this->returnValue('some'));
        
        $this->cache = new CacheKey($mockCacher, 'any');
        $this->cache->set('some');
        $this->assertSame('some', $this->cache->get());
    }
    
    public function testDel()
    {
        $mockCacher = $this->getMock('Cacher', array('del'));
        
        $mockCacher->expects($this->any())
                ->method('del')
                ->with('some')
                ->will($this->returnValue(true));
        
        $cache = new CacheKey($mockCacher, 'some');
        $this->assertTrue($cache->del());
    }

}