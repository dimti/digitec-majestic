<?php

/*
 * @copyright NetMonsters <team@netmonsters.ru>
 * @link http://netmonsters.ru
 * @package Majestic
 * @subpackage UnitTests
 * @since 2011-10-25
 * 
 * Unit tests for Cacher class
 */


require_once dirname(__FILE__) . '/../../cache/Cache.php';
require_once dirname(__FILE__) . '/../../cache/Cacher.php';
require_once dirname(__FILE__) . '/../../Registry.php';
require_once dirname(__FILE__) . '/../../Config.php';
require_once dirname(__FILE__) . '/../../exception/InitializationException.php';

class CacherTest extends PHPUnit_Framework_TestCase
{

    private $mock;
    
    protected function setUp()
    {
        set_new_overload(array($this, 'newCallback'));
    }

    public function testNotExtendsCache()
    {
        $this->setExpectedException('InitializationException', 'Cache driver');
        Cacher::get('Cacher');
    }

    public function testGet()
    {        
        $this->mock = $this->getMockForAbstractClass(
                'Cache',                                      /* name of class to mock     */ 
                array(),                               /* list of methods to mock   */ 
                'CacheMock'                               /* name for mocked class     */
        );
        
        $this->assertTrue(Cacher::get('Cache') instanceof Cache);
    }
    
    public function testCacheAlreadySet()
    {
        $this->assertTrue(Cacher::get('Cache') instanceof Cache);        
    }

    protected function newCallback($className)
    {
        switch ($className) {
            case 'Cache':
                return 'CacheMock';
            default:
                return $className;
        }
    }

    public function tearDown()
    {
        unset_new_overload();
    }

}

