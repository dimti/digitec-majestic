<?php

/*
 * @copyright NetMonsters <team@netmonsters.ru>
 * @link http://netmonsters.ru
 * @package Majestic
 * @subpackage UnitTests
 * @since 2011-10-28
 * 
 * Unit tests for CommandProfiler class
 */

require_once dirname(__FILE__) . '/../../../util/profiler/CommandProfiler.php';

class CommandProfilerTest extends PHPUnit_Framework_TestCase
{

    private $profiler;

    public function setUp()
    {
        $this->profiler = new CommandProfiler('method', 'exec');
        $this->profiler->end();
    }

    public function testGetEllapsed()
    {
        $this->assertGreaterThan(0, $this->profiler->getElapsed());
    }

    public function testGetType()
    {
        $this->assertSame('method', $this->profiler->getType());
        $this->assertNotEquals('argument', $this->profiler->getType());
    }

    public function testGetCommand()
    {
        $this->assertSame('exec', $this->profiler->getCommand());
        $this->assertNotEquals('grep', $this->profiler->getCommand());
    }
}