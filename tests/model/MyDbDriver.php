<?php

abstract class MyDbDriver extends DbDriver
{
    public function getInsertId($table = null, $key = null)
    {
        return true;
    }

    public function quoteIdentifier($param)
    {
        return $param;
    }

    public function quote($param)
    {
        return $param;
    }

    public function insert($table, $bind, $on_duplicate = array())
    {
        return $table;
    }

    public function update($table, $bind, $where = '')
    {
        return $table;
    }

    public function delete($table, $where = '')
    {
        return $table;
    }

    public function query($sql, $params = array())
    {
        $conf = array('driver' => 'MockDbDriver', 'hostname' => 'somehost', 'database' => 'db', 'username' => 'test', 'password' => '1234');
        return new MockDbDriver($conf);
    }

    public function execute()
    {
        return true;
    }

    public function fetch()
    {
        return true;
    }

    public function fetchField($field)
    {
        return $field;
    }

    public function fetchAll()
    {
        return true;
    }
}