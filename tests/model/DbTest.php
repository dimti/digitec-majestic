<?php

/*
 * @copyright NetMonsters <team@netmonsters.ru>
 * @link http://netmonsters.ru
 * @package Majestic
 * @subpackage UnitTests
 * @since 2011-11-1
 * 
 * Unit tests for Db class
 */

require_once dirname(__FILE__) . '/../../Registry.php';
require_once dirname(__FILE__) . '/../../Config.php';
require_once dirname(__FILE__) . '/../../model/DbDriver.php';
require_once dirname(__FILE__) . '/../../model/Db.php';
require_once dirname(__FILE__) . '/../../exception/GeneralException.php';
require_once dirname(__FILE__) . '/../../exception/InitializationException.php';

class DbTest extends PHPUnit_Framework_TestCase
{
    public function testConnectNoParams()
    {
       $this->setExpectedException('InitializationException', 'Trying to get property of non-object');
       Db::connect();
    }
    public function testConnectConfigNotArray()
    {
        $this->setExpectedException('InitializationException', 'Connection parameters must be an array');
        Db::connect('name', 'config');
    }

    /**
     * @group MySQL
     */
    public function testConnectGlobalConfig()
    {
        $conf = array('hostname' => 'localhost', 'driver' => 'MySQLiDriverGlobalConfMock', 'database' => 'db', 'username' => 'test', 'password' => '1234');
        $this->getMockForAbstractClass('DbDriver', array(), 'MySQLiDriverGlobalConfMock', false);
        Config::set('Db', array('global' =>$conf));
        $driver = Db::connect('global');
        $this->assertInstanceOf('DbDriver', $driver);
    }

    /**
     * @group MySQL
     */
    public function testConnectWithConfigParam()
    {
        $conf = array('hostname' => 'localhost', 'driver' => 'MySQLiDriverMock', 'database' => 'db', 'username' => 'test', 'password' => '1234');
        $this->getMockForAbstractClass('DbDriver', array(), 'MySQLiDriverMock', false);
        $driver = Db::connect('mysql', $conf);
        $this->assertInstanceOf('DbDriver', $driver);
    }

    public function testConnectWithWrongDriver()
    {
        $this->getMock('NotDbDriver', array(), array(), 'NoDbDriverMock');
        $this->setExpectedException('InitializationException', 'Database driver must extends DbDriver');
        $driver = Db::connect('nodb', array('hostname' => 'localhost', 'driver' => 'NoDbDriverMock'));
    }
}