<?php

/*
 * @copyright NetMonsters <team@netmonsters.ru>
 * @link http://netmonsters.ru
 * @package Majestic
 * @subpackage UnitTests
 * @since 2011-11-3
 * 
 * Unit tests for DbExpr class
 */

require_once dirname(__FILE__) . '/../../model/DbExpr.php';

class DbExprTest extends PHPUnit_Framework_TestCase
{
    public function testExpr()
    {
        $expr = new DbExpr('THIS IS SQL EXPRESSION');
        $str = (string) $expr;
        $this->assertSame('THIS IS SQL EXPRESSION', $str);
    }
}