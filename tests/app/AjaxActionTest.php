<?php

/*
 * @copyright NetMonsters <team@netmonsters.ru>
 * @link http://netmonsters.ru
 * @package Majestic
 * @subpackage UnitTests
 * @since 2011-11-01
 * 
 * Unit tests for AjaxAction class
 */

require_once dirname(__FILE__) . '/Action_TestCase.php';
require_once dirname(__FILE__) . '/../../app/AjaxAction.php';

class AjaxActionTest extends Action_TestCase
{

    /**
     * @runInSeparateProcess
     */
    public function testConstruct()
    {
        Config::set('DEBUG', false);
        Env::setParams(array('ajax' => 'AjaxTemplate', 'param2' => 'value2'));
        $action = $this->getMockForAbstractClass('AjaxAction');
        $this->assertAttributeEquals('ajax', 'template', $action);
    }

    /**
     * @runInSeparateProcess
     */
    public function testFetchWithEncode()
    {
        Config::set('DEBUG', false);
        $controller = FrontController::getInstance();
        $controller->setView('SomeView');
        $action = $this->getMockForAbstractClass('AjaxAction');
        $action->data = array('var' => 'val');
        $result = $action->fetch();
        $this->assertSame('/actions//ajax', $result['template']);
        $this->assertSame('{"var":"val"}', $result['data']);
    }

    /**
     * @runInSeparateProcess
     */
    public function testFetchNoEncode()
    {
        Config::set('DEBUG', false);
        Env::setParams(array('json_encode' => false));
        $controller = FrontController::getInstance();
        $controller->setView('SomeView');
        $action = $this->getMockForAbstractClass('AjaxAction');
        $action->data = array('var' => 'val');
        $result = $action->fetch();
        $this->assertSame('/actions//ajax', $result['template']);
        $this->assertSame('Array', (string) $result['data']);
        $action->data = 'stringvalue';
        $result = $action->fetch();
        $this->assertSame('/actions//ajax', $result['template']);
        $this->assertSame('stringvalue', (string) $result['data']);
    }

    /**
     * @runInSeparateProcess
     */
    public function testFetchWithEncodeDefault()
    {
        Config::set('DEBUG', false);
        $controller = FrontController::getInstance();
        $controller->setView('SomeView');
        $action = $this->getMockForAbstractClass('AjaxAction');
        $result = $action->fetch();
        $this->assertSame('/actions//ajax', $result['template']);
        $this->assertSame('false', (string) $result['data']);
    }

    /**
     * @runInSeparateProcess
     */
    public function testFetchNoEncodeDefault()
    {
        Config::set('DEBUG', false);
        Env::setParams(array('json_encode' => false));
        $controller = FrontController::getInstance();
        $controller->setView('SomeView');
        $action = $this->getMockForAbstractClass('AjaxAction');
        $result = $action->fetch();
        $this->assertSame('/actions//ajax', $result['template']);
        $this->assertSame('', (string) $result['data']);
    }
}