<?php

/*
 * @copyright NetMonsters <team@netmonsters.ru>
 * @link http://netmonsters.ru
 * @package Majestic
 * @subpackage UnitTests
 * @since 2011-11-1
 * 
 * Unit tests for StaticAction class
 */

require_once dirname(__FILE__) . '/Action_TestCase.php';
require_once dirname(__FILE__) . '/../../app/StaticAction.php';

class StaticActionTest extends Action_TestCase
{

    /**
     * @runInSeparateProcess
     */
    public function testFetchNoTemplate()
    {

        Config::set('DEBUG', false);
        Env::setParams(array('template' => ''));
        $controller = FrontController::getInstance();
        $controller->setView('SomeView');
        $action = $this->getMockForAbstractClass('StaticAction', array(), 'StaticActionMock');
        $result = $action->fetch();
        $this->assertSame('/static/StaticActi', $result['template']);
    }

    /**
     * @runInSeparateProcess
     */
    public function testFetchWithTemplate()
    {

        Config::set('DEBUG', false);
        Env::setParams(array('template' => 'SomeTemplate'));
        $controller = FrontController::getInstance();
        $controller->setView('SomeView');
        $action = $this->getMockForAbstractClass('StaticAction', array(), 'StaticActionMock');
        $result = $action->fetch();
        $this->assertSame('/static/SomeTemplate', $result['template']);
    }
}