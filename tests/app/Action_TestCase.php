<?php

/*
 * @copyright NetMonsters <team@netmonsters.ru>
 * @link http://netmonsters.ru
 * @package Majestic
 * @subpackage UnitTests
 * @since 2011-11-1
 * 
 * Action_TestCase class for testing Actions
 */

require_once dirname(__FILE__) . '/../../Registry.php';
require_once dirname(__FILE__) . '/../../Config.php';
require_once dirname(__FILE__) . '/../../Load.php';
require_once dirname(__FILE__) . '/../../classes/Env.class.php';
require_once dirname(__FILE__) . '/../../exception/ErrorHandler.php';
require_once dirname(__FILE__) . '/../../app/FrontController.php';
require_once dirname(__FILE__) . '/../../app/Action.php';
require_once dirname(__FILE__) . '/../../view/iView.php';

class Action_TestCase extends PHPUnit_Framework_TestCase
{

    public function run(PHPUnit_Framework_TestResult $result = NULL)
    {
        $this->setPreserveGlobalState(false);
        return parent::run($result);
    }

    public function setUp()
    {
        $this->getMock('Router');
        $this->getMock('PHPView', array('fetch', 'assignObject'));
    }

    public function tearDown()
    {
        $env = new ReflectionClass('Env');
        $params = $env->getProperty('params');
        $params->setAccessible(true);
        $params->setValue('Env', array());
    }
}

class SomeView implements iView
{
    private $result = array();
    public function fetch($template)
    {
        $this->result['template'] = $template;
        return $this->result;
    }

    public function assignObject() {}

    public function assign($name, $value = null) {
        $this->result[$name] = $value;
    }

    public function prepend($name, $value)
    {

    }

    public function append($name, $value)
    {

    }
}