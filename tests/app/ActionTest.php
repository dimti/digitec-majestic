<?php

/*
 * @copyright NetMonsters <team@netmonsters.ru>
 * @link http://netmonsters.ru
 * @package Majestic
 * @subpackage UnitTests
 * @since 2011-11-1
 * 
 * Unit tests for Action class
 */

require_once dirname(__FILE__) . '/Action_TestCase.php';

class ActionTest extends Action_TestCase
{

    /**
     * @runInSeparateProcess
     */
    public function testActionConstructWithParams()
    {
        Config::set('DEBUG', false);
        Env::setParams(array('param1' => 'value1', 'param2' => 'value2'));
        $action = $this->getMockForAbstractClass('Action' );
        $this->assertSame('value1', $action->param1);
    }

    /**
     * @runInSeparateProcess
     */
    public function testFetch()
    {
        Config::set('DEBUG', false);
        $load = new ReflectionClass('Load');
        $classes = $load->getProperty('autoload');
        $classes->setAccessible(true);
        $classes->setValue('autoload', array('ActionMock' =>'some/path/to/action.php'));
        Env::setParams(array('template' => 'SomeTemplate', 'param2' => 'value2'));
        $controller = FrontController::getInstance();
        $controller->setView('SomeView');
        $action = $this->getMockForAbstractClass('Action', array(), 'ActionMock');
        $result = $action->fetch();
        $this->assertSame('/actions/to/SomeTemplate', $result['template']);
    }

    /**
     * @runInSeparateProcess
     */
    public function testFetchNoTemplate()
    {
        Config::set('DEBUG', false);
        Env::setParams(array('template' => ''));
        $controller = FrontController::getInstance();
        $controller->setView('SomeView');
        $action = $this->getMockForAbstractClass('Action', array(), 'ActionMock');
        $result = $action->fetch();
        $this->assertSame('/actions//Acti', $result['template']);
    }

    /**
     * @runInSeparateProcess
     */
    public function testRedirect()
    {
        set_exit_overload(function() { return false; });

        Config::set('DEBUG', false);
        $load = new ReflectionClass('Action');
        $redirect = $load->getMethod('redirect');
        $redirect->setAccessible(true);
        $action = $this->getMockForAbstractClass('Action', array(), 'ActionMock');
        $this->assertNull($redirect->invoke($action, '/some/url'));
        unset_exit_overload();
    }
}