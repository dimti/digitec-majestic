<?php

/*
 * @copyright NetMonsters <team@netmonsters.ru>
 * @link http://netmonsters.ru
 * @package Majestic
 * @subpackage UnitTests
 * @since 2011-10-31
 * 
 * Unit tests for Router class
 */

require_once dirname(__FILE__) . '/../../../app/router/Route.php';

class RouteTest extends PHPUnit_Framework_TestCase
{

    public function testConstructNoParams()
    {
        $route = new Route('index', 'user');
        $this->assertSame('userAction', $route->getAction());
        $this->assertSame('Layout', $route->getLayout());
        $this->assertEmpty($route->getParams());
        $this->assertAttributeEquals('index', 'route', $route);
    }

    public function testConstructWithParams()
    {
        $route = new Route('index', 'user', array('name' => 'tony', 'role' => 'user'));
        $this->assertArrayHasKey('role', $route->getParams());
        $this->assertSame(array('name' => 'tony', 'role' => 'user'), $route->getParams());
    }

    public function testMatchDiffCount()
    {
        $route = new Route('user/account/id/142', 'user', array('name' => 'tony', 'role' => 'user'));
        $this->assertFalse($route->match(array('user', 'account')));
    }

    /**
     * @TODO: Route::match() - need to trim left-right slashes
     */
    public function testMatchDiffParts()
    {
        $route = new Route('user/account', 'user');
        $this->assertFalse($route->match(array('user', 'login')));
    }

    public function testMatchVariable()
    {
        $route = new Route('user/account/:id', 'user');
        $this->assertTrue($route->match(array('user', 'account', '142')));
        $this->assertSame(array('id' => '142'), $route->getParams());
    }

    public function testMatchDiffPregParts()
    {
        $route = new Route('user/account/(?<key>value)', 'user');
        $this->assertFalse($route->match(array('user', 'account', 'wrong')));
        $this->assertSame(0, count($route->getParams()));
    }

    public function testMatchPregPasses()
    {
        $route = new Route('user/account/(?<key>[a-z]+)', 'user');
        $this->assertTrue($route->match(array('user', 'account', 'value')));
        $this->assertSame(array('key' => 'value'), $route->getParams());
        $this->assertSame(1, count($route->getParams()));
    }

    /**
     * @TODO Need check empty parameters from constructor Route
     */
    public function testMatchEmptyRequest()
    {
        $route = new Route('', 'user', array('name' => 'tony', 'role' => 'user'));
        $this->setExpectedException('PHPUnit_Framework_Error');
        $route->match('');
    }

    public function testGetUri()
    {
        $route = 'myroute';
        $route_mock = $this->getMockBuilder('Route')
            ->disableOriginalConstructor()
            ->setMethods(array('__construct'))
            ->getMock();
        $reflection = new ReflectionProperty('Route', 'route');
        $reflection->setAccessible(true);
        $reflection->setValue($route_mock, $route);
        $this->assertEquals('/' . $route, $route_mock->getUri());
    }
}