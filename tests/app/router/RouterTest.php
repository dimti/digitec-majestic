<?php

/*
 * @copyright NetMonsters <team@netmonsters.ru>
 * @link http://netmonsters.ru
 * @package Majestic
 * @subpackage UnitTests
 * @since 2011-10-31
 * 
 * Unit tests for Router class
 */

require_once dirname(__FILE__) . '/../../../classes/Env.class.php';
require_once dirname(__FILE__) . '/../../../app/router/Route.php';
require_once dirname(__FILE__) . '/../../../app/router/Router.php';

class RouterTest extends PHPUnit_Framework_TestCase
{
    public function testGetRouteFirst()
    {
        $router = new Router();
        $this->assertNull($router->getRoute());
        $this->assertNull($router->getRouteName());
    }

    public function testRouterCycle()
    {
        $router = new Router();
        $router->add('user', 'user/account/:id', 'user');
        $route = $router->route('user/account/213');
        $this->assertSame(1, count($route->getParams()));
        $this->assertSame(array('id' => '213'), $route->getParams());
        $this->assertSame('user', $router->getRouteName());
    }

    public function testRouterMultipleRoutes()
    {
        $router = new Router();
        $router->add('user', 'user/account/:id', 'user');
        $router->add('sale', 'sale/order/:id', 'user');
        $route = $router->route('user/account/213');
        $this->assertSame('user', $router->getRouteName());
        $this->assertSame(1, count($route->getParams()));
        $this->assertSame(array('id' => '213'), $route->getParams());
        $route = $router->route('sale/order/22');
        $this->assertSame('sale', $router->getRouteName());
        $this->assertSame(array('id' => '22'), $route->getParams());
    }

    public function testRouteNotmatch()
    {
        $router = new Router();
        $router->add('user', 'user/account/:id', 'user');
        $this->assertFalse($router->route('user/info/213'));

    }

    public function testSetDefaultLayout()
    {
        $router = new Router();
        $router->setDefaultLayout('userLayout');
        $this->assertAttributeEquals('userLayout', 'default_layout', $router);
    }

    public function testGetRouteWithNameIsNull()
    {
        $name = null;
        $route = 'route object.';
        $router = new Router();
        $reflection = new ReflectionProperty('Router', 'route');
        $reflection->setAccessible(true);
        $reflection->setValue($router, $route);
        $this->assertEquals($route, $router->getRoute($name));
    }

    public function testGetRouteWithNamed()
    {
        $name = 'nameofroute';
        $uri = 'uri from route.';
        $route = 'route object.';
        $router = new Router();
        $reflection = new ReflectionProperty('Router', 'routes');
        $reflection->setAccessible(true);
        $reflection->setValue($router, array($name => $route));
        $this->assertEquals($route, $router->getRoute($name));
    }

    public function testGetRouteWithNamedWithError()
    {
        $name = 'name of route';
        $router = new Router();
        $this->setExpectedException('ErrorException');
        $router->getRoute($name);
    }

    public function testRouteIsExists()
    {
        $route = 'route object.';
        $name = 'nameofroute';
        $name_is_not_exists = 'nameofroutenotexists';
        $routes = array($name => $route);
        $router = new Router();
        $reflection = new ReflectionProperty('Router', 'routes');
        $reflection->setAccessible(true);
        $reflection->setValue($router, $routes);
        $this->assertTrue($router->routeIsExists($name));
        $this->assertFalse($router->routeIsExists($name_is_not_exists));
    }

    public function testGetDefaultErrorLayout()
    {
        $router = new Router();
        $this->assertSame('ErrorLayout', $router->getErrorLayout());
    }

    public function testSetErrorLayout()
    {
        $router = new Router();
        $router->setErrorLayout('CustomError');
        $this->assertSame('CustomErrorLayout', $router->getErrorLayout());
    }
}
