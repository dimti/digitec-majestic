<?php

/*
 * @copyright NetMonsters <team@netmonsters.ru>
 * @link http://netmonsters.ru
 * @package Majestic
 * @subpackage UnitTests
 * @since 2011-11-1
 * 
 * Unit tests for PagerAction class
 */

require_once dirname(__FILE__) . '/Action_TestCase.php';
require_once dirname(__FILE__) . '/../../app/PagerAction.php';

class PagerActionTest extends Action_TestCase
{

    /**
     * @runInSeparateProcess
     */
    public function testConstructWithParams()
    {

        Config::set('DEBUG', false);
        $action = $this->getMockForAbstractClass('PagerAction');
        $this->assertSame(20, $action->getLimit());
        $action = $this->getMockForAbstractClass('PagerAction', array(50));
        $this->assertSame(50, $action->getLimit());
    }

    /**
     * @runInSeparateProcess
     */
    public function testSetCount()
    {

        Config::set('DEBUG', false);
        $action = $this->getMockForAbstractClass('PagerAction');
        $action->setCount(50);
        $this->assertSame(1, $action->page);
        $this->assertSame(0, $action->getOffset());
        $_GET['p'] = 'last';
        $action->setCount(50);
        $this->assertSame(3.0, $action->page);
        $this->assertSame(40, $action->getOffset());
        $_GET['p'] = 2;
        $action->setCount(50);
        $this->assertSame(2, $action->page);
        $this->assertSame(20, $action->getOffset());
        $_GET['p'] = -3;
        $action->setCount(50);
        $this->assertSame(1, $action->page);
    }

    /**
     * @runInSeparateProcess
     */
    public function testGetOffset()
    {

        Config::set('DEBUG', false);
        $action = $this->getMockForAbstractClass('PagerAction');
        $this->assertSame(0, $action->getOffset());
    }

    /**
     * @runInSeparateProcess
     */
    public function testFetchNoTemplate()
    {

        Config::set('DEBUG', false);
        Env::setParams(array('template' => ''));
        $controller = FrontController::getInstance();
        $controller->setView('SomeView');
        $action = $this->getMockForAbstractClass('PagerAction', array(), 'PagerActionMock');
        $result = $action->fetch();
        $this->assertSame('/actions/PagerActi', $result['template']);
    }

    /**
     * @runInSeparateProcess
     */
    public function testFetchWithTemplate()
    {

        Config::set('DEBUG', false);
        Env::setParams(array('template' => 'SomeTemplate'));
        $controller = FrontController::getInstance();
        $controller->setView('SomeView');
        $action = $this->getMockForAbstractClass('PagerAction');
        $result = $action->fetch();
        $this->assertSame('/actions/SomeTemplate', $result['template']);
    }
}