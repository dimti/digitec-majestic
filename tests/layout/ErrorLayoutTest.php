<?php
/**
 * @copyright NetMonsters <team@netmonsters.ru>
 * @link http://netmonsters.ru
 * @package Majestic
 * @subpackage UnitTests
 * @since 04-06-2012
 * @user: agrebnev
 */

require_once dirname(__FILE__) . '/../../Registry.php';
require_once dirname(__FILE__) . '/../../Config.php';
require_once dirname(__FILE__) . '/../../exception/ErrorHandler.php';
require_once dirname(__FILE__) . '/../../exception/GeneralException.php';
require_once dirname(__FILE__) . '/../../app/FrontController.php';
require_once dirname(__FILE__) . '/../../layout/Layout.php';
require_once dirname(__FILE__) . '/../../layout/Error.layout.php';

/**
 * @runTestsInSeparateProcesses
 */
class ErrorLayoutTest extends PHPUnit_Framework_TestCase
{

    public function run(PHPUnit_Framework_TestResult $result = NULL)
    {
        $this->setPreserveGlobalState(false);
        return parent::run($result);
    }

    public function setUp()
    {
        if (!class_exists('RouterMock')) {
            $this->getMock('Router', array(), array(), 'RouterMock', false);
        }
        if (!class_exists('PHPViewMock')) {
            $this->getMock('PHPView', array('fetch', 'append', 'prepend', 'assign', 'getTemplate'), array(), 'PHPViewMock', false);
        }

        set_new_overload(array($this, 'newCallback'));
    }

    public function testSetException()
    {

        Config::set('DEBUG', false);
        $layout = new ErrorLayout();
        $layout->setException(new GeneralException());
        $this->assertAttributeInstanceOf('GeneralException', 'exception', $layout);
    }

    public function testExecute()
    {

        Config::set('DEBUG', false);
        $action = $this->getMock('Action', array('fetch'));
        $action->expects($this->once())
                ->method('fetch')
                ->will($this->returnValue('this is the result of Action::fetch()'));

        $layout = new ErrorLayout();
        $layout->fetch($action);
    }


    protected function newCallback($className)
    {
        switch ($className) {
            case 'Router':
                return 'RouterMock';
            case 'PHPView':
                return 'PHPViewMock';
            default:
                return $className;
        }
    }

    public function tearDown()
    {
        unset_new_overload();
    }
}
