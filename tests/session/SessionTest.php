<?php

/*
 * @copyright NetMonsters <team@netmonsters.ru>
 * @link http://netmonsters.ru
 * @package Majestic
 * @subpackage UnitTests
 * @since 2011-10-25
 * 
 * Unit tests for Session class
 * @TODO: Session::destroy() - check if session was started
 * @TODO: Session::destroy() - uncheck started flag after destroy
 */

require_once dirname(__FILE__) . '/../../session/Session.php';

class SessionTest extends PHPUnit_Framework_TestCase
{

    private $started = null;

    protected function setUp()
    {
        $class = new ReflectionClass('Session');
        $this->started = $class->getProperty('started');
        $this->started->setAccessible(true);
        Session::start();
        $sid = Session::getId();
        if (!empty($sid)) {
            Session::destroy();
        }
        $this->started->setValue(null, false);
    }

    public function testStart()
    {
        Session::start();
        $this->assertAttributeEquals(true, 'started', 'Session');
    }

    public function testSetGet()
    {
        Session::set('one', 1);
        Session::set('two', 'three');
        Session::set(array('first' => '1st', 'second' => '2nd'));
        $this->assertSame('1st', Session::get('first'));
        $this->assertSame('three', Session::get('two'));
        $this->assertNotEquals('three', Session::get('thr'));
    }

    public function testNullKey()
    {
        $this->assertNull(Session::get());
        Session::start();
        $this->assertEmpty(Session::get());
    }

    public function testReturnDefaultValue()
    {
        Session::start();
        $this->assertSame(1, Session::get('key', 1));
    }

    public function testDestroyedGet()
    {
        $this->assertFalse($this->started->getValue());
        $_COOKIE[session_name()] = session_name();
        $this->assertSame(1, Session::get('key', 1));
    }

    public function testDel()
    {
        Session::set('one', 1);
        Session::set('two', 'three');
        $this->assertSame('three', Session::get('two'));
        Session::del('two');
        $this->assertNull(Session::get('two'));
    }

    public function testDestroyedDel()
    {
        Session::del('two');
        $this->assertNull(Session::get('two'));
        $this->assertFalse($this->started->getValue());
        $_COOKIE[session_name()] = session_name();
        Session::del('two');
        $this->assertNull(Session::get('two'));
    }

    public function testRegenerateId()
    {
        $this->assertEmpty(session_id());
        Session::start();
        $ssid = Session::getId();
        $this->assertNotEmpty($ssid);
        Session::regenerateId();
        $new_ssid = Session::getId();
        $this->assertNotEmpty($new_ssid);
        $this->assertNotEquals($new_ssid, $ssid);
    }

    public function testRememberUntil()
    {
        Session::start();
        $ssid = Session::getId();
        $params = session_get_cookie_params();
        Session::rememberUntil(400);
        $new_ssid = Session::getId();
        $new_params = session_get_cookie_params();
        $this->assertNotEquals($ssid, $new_ssid);
        $this->assertNotEquals($params, $new_params);
        $this->assertSame(400, $new_params['lifetime']);
        Session::rememberUntil();
        $new_params = session_get_cookie_params();
        $this->assertSame(0, $new_params['lifetime']);
    }

    public function testForget()
    {
        Session::start();
        $ssid = Session::getId();
        Session::forget();
        $new_ssid = Session::getId();
        $new_params = session_get_cookie_params();
        $this->assertNotEquals($ssid, $new_ssid);
        $this->assertSame(0, $new_params['lifetime']);
    }

    public function testRemember()
    {
        Session::start();
        $ssid = Session::getId();
        Session::remember();
        $new_params = session_get_cookie_params();
        $this->assertSame(1209600, $new_params['lifetime']);

        Session::remember(-30);
        $new_params = session_get_cookie_params();
        $this->assertSame(1209600, $new_params['lifetime']);

        Session::remember(530);
        $new_params = session_get_cookie_params();
        $this->assertSame(530, $new_params['lifetime']);
    }

    public function testExpireSessionCookie()
    {
        Session::start();
        $params = session_get_cookie_params();
        $_COOKIE[session_name()] = true;
        Session::expireSessionCookie();
        $this->assertNotNull($_COOKIE);
    }

    public function testSetSessionHandler()
    {
        Session::setSessionHandler('Handler');
        $this->assertTrue(TRUE);
    }
}

class Handler
{
    public static function open()
    {
    }

    public static function close()
    {
    }

    public static function read()
    {
    }

    public static function write()
    {
    }

    public static function destroy()
    {
    }

    public static function gc()
    {
    }
}