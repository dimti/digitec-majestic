<?php

/*
 * @copyright NetMonsters <team@netmonsters.ru>
 * @link http://netmonsters.ru
 * @package Majestic
 * @subpackage UnitTests
 * @since 2011-11-15
 *
 * Unit tests for SessionModel class
 */

require_once dirname(__FILE__) . '/../../Registry.php';
require_once dirname(__FILE__) . '/../../Config.php';
require_once dirname(__FILE__) . '/../../classes/Env.class.php';
require_once dirname(__FILE__) . '/../../model/Db.php';
require_once dirname(__FILE__) . '/../../model/DbDriver.php';
require_once dirname(__FILE__) . '/../model/MyDbDriver.php';
require_once dirname(__FILE__) . '/../../model/Model.php';
require_once dirname(__FILE__) . '/../../model/SqlModel.php';
require_once dirname(__FILE__) . '/../../session/Session.model.php';

class SessionModelTest extends PHPUnit_Framework_TestCase
{

    protected $model;

    public function setUp()
    {
        $conf = array('default' => array('driver' => 'MockDbDriver', 'hostname' => 'somehost', 'database' => 'db', 'username' => 'test', 'password' => '1234'));
        if (!class_exists('MockDbDriver')) {
            $this->getMockForAbstractClass('MyDbDriver', array($conf), 'MockDbDriver', false);
        }
        if (!class_exists('MockDbExpr')) {
            $this->getMock('DbExpr', array(), array(), 'MockDbExpr', false);
        }

        Config::set('Db', $conf);

        set_new_overload(array($this, 'newCallback'));
    }

    public function testOpen()
    {
        $this->model = new SessionModel();
        $this->assertTrue($this->model->open('path', 'name'));
    }

    public function testClose()
    {
        $this->model = new SessionModel();
        $this->assertTrue($this->model->close());
    }

    public function testRead()
    {
        $this->model = new SessionModel();
        $this->assertEquals('data', $this->model->read(1));
    }

    public function testWrite()
    {
        $this->model = new SessionModel();
        $_SERVER['REMOTE_ADDR'] = '127.0.0.1';
        $this->assertEmpty(Env::Server('HTTP_X_FORWARDED_FOR'));
        
        $this->assertTrue($this->model->write(2, 'user|.s:20;id=2;id=2'));
    }

    public function testDestroy()
    {
        $this->model = new SessionModel();
        $this->assertTrue($this->model->destroy(2));
    }
    public function testGc()
    {
        $this->model = new SessionModel();
        $this->assertTrue($this->model->gc(2000));
    }

    public function testDestroyByUserId()
    {
        $this->model = new SessionModel();
        $this->assertEquals('session', $this->model->destroyByUserId(12));
    }

    public function tearDown()
    {
        Config::getInstance()->offsetUnset('Db');
        $config = new ReflectionClass('Db');
        $registry = $config->getProperty('connections');
        $registry->setAccessible(true);
        $registry->setValue('Db', array());
        unset_new_overload();
    }

    protected function newCallback($className)
    {
        switch ($className) {
            case 'DbExpr':
                return 'MockDbExpr';
            case 'MockDbDriver':
                return 'MockDbDriver';
            case 'CacheKey':
                return 'MockCacheKey';
            default:
                return $className;
        }
    }
}
