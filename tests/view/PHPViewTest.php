<?php

/*
 * @copyright NetMonsters <team@netmonsters.ru>
 * @link http://netmonsters.ru
 * @package Majestic
 * @subpackage UnitTests
 * @since 2011-10-11
 * 
 * Unit tests for PHPView class
 */

require_once dirname(__FILE__) . '/../../view/iView.php';
require_once dirname(__FILE__) . '/../../Registry.php';
require_once dirname(__FILE__) . '/../../view/helpers/ViewHelper.php';
require_once dirname(__FILE__) . '/../../view/helpers/TitleViewHelper.php';
require_once dirname(__FILE__) . '/../../view/PHPView.php';
require_once dirname(__FILE__) . '/../../exception/GeneralException.php';
require_once dirname(__FILE__) . '/../../exception/InitializationException.php';
require_once 'vfsStream/vfsStream.php';

class PHPViewTest extends PHPUnit_Framework_TestCase
{

    private $view;
    private $template;

    public function setUp()
    {
        vfsStreamWrapper::register();
        vfsStream::setup();
        $root = vfsStream::create(array());
        vfsStreamWrapper::setRoot($root);
        $views_dir = vfsStream::newDirectory('views');
        $this->template = new vfsStreamFile('test.phtml');
        $this->template->setContent('<?php echo $a ." " . $b . " " . $c; ?>');
        $views_dir->addChild($this->template);
        $root->addChild($views_dir);

        $this->view = new PHPView(array('path' => vfsStream::url('root/views/')));
    }

    public function testPHPViewConstructor()
    {
        $this->assertInstanceOf('PHPView', $this->view);
        $this->assertSame('vfs://root/views/', $this->view->getPath());
    }


    public function testPHPViewNullConstructor()
    {
        $this->setExpectedException('InitializationException', 'Configuration must have a "path" set.');
        $view = new PHPView(null);
    }

    public function testAssign()
    {
        $this->view->assign('a', 'c');

        $this->view->append('b', 'b');
        $this->view->prepend('c', 'a');

        $this->assertStringStartsWith('c b a', $this->view->fetch('test'));
    }

    public function testAssignObject()
    {
        $obj = $this->getMock('NewClass');
        $obj->a = 'one';
        $obj->b = 'two';
        $obj->c = 'three';
        $this->view->assignObject($obj);
        $this->assertSame('one two three', $this->view->fetch('test'));
    }

    public function testEscape()
    {
        $result = $this->view->escape('"<>"');
        $this->assertSame('&quot;&lt;&gt;&quot;', $result);
    }

    public function testCall()
    {
        $this->view->title('New title');
        $this->assertContains('New title', Registry::get('TitleViewHelper'));
    }


    public function testIllegalCall()
    {
        $this->setExpectedException('GeneralException', 'View helper "WriteViewHelper" not found.');
        $this->view->write('tony');
    }

    /**
     * @TODO: PHPView: check views path for ending slash in getTemplatePath()
     */
    public function testFetch()
    {

        $this->assertNotSame('test phtml view ', $this->template->getContent());

        $this->view->assign('a', 'some');
        $this->view->assign('b', 'some');
        $this->view->assign('c', 'some');

        $result = $this->view->fetch('test');

        $this->assertNotEmpty($result);
        $this->assertSame('some some some', $result);
    }

    public function testAppend()
    {        
        $this->view->assign('a', 'some');
        $this->view->append('b', 'some');
        $this->view->assign('c', 'some');
        
        $this->view->append('c', 'end');

        $result = $this->view->fetch('test');
        $this->assertSame('some some someend', $result);
    }

    public function testPrepend()
    {
        $this->view->assign('a', 'some');
        $this->view->prepend('b', 'some');
        $this->view->assign('c', 'some');
        
        $this->view->prepend('c', 'start');

        $result = $this->view->fetch('test');
        $this->assertSame('some some startsome', $result);
        
    }

    public function testErrorTemplate()
    {
        $view = new PHPView(array('path' => 'error_path/'));
        $this->setExpectedException('GeneralException', 'Template');
        $result = $view->fetch('test');
    }

}