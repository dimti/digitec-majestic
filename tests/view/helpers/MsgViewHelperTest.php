<?php

/*
 * @copyright NetMonsters <team@netmonsters.ru>
 * @link http://netmonsters.ru
 * @package Majestic
 * @subpackage UnitTests
 * @since 2011-10-11
 * 
 * Unit tests for PHPView class
 */

require_once dirname(__FILE__) . '/../../../session/Session.php';
require_once dirname(__FILE__) . '/../../../view/iView.php';
require_once dirname(__FILE__) . '/../../../view/PHPView.php';
require_once dirname(__FILE__) . '/../../../view/helpers/ViewHelper.php';
require_once dirname(__FILE__) . '/../../../view/helpers/MsgViewHelper.php';
require_once dirname(__FILE__) . '/../../../exception/GeneralException.php';

class MsgViewHelperTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var MsgViewHelper
     */
    public $helper;

    public function setUp()
    {
        Session::del('MsgViewHelper');
        $this->helper = new MsgViewHelper(new PHPView(array('path' => 'any')));
    }

    public function testMsg()
    {

        $this->helper->msg('new message from test', 'success');
        $this->assertSame(array('message' => 'new message from test', 'type' => 'success'), Session::get('MsgViewHelper'));

        $this->assertSame($this->helper, $this->helper->msg('error message', 'error'));
        $this->assertSame(array('message' => 'error message', 'type' => 'error'), Session::get('MsgViewHelper'));
    }

    public function testWrongType()
    {
        $this->setExpectedException('GeneralException', 'Unknown message type');
        $this->helper->msg('some message', 'wrong');
    }

    public function testSuccess()
    {
        $this->helper->success('success message');
        $this->assertSame(array('message' => 'success message', 'type' => 'success'), Session::get('MsgViewHelper'));
    }

    public function testError()
    {
        $this->helper->error('error message');
        $this->assertSame(array('message' => 'error message', 'type' => 'error'), Session::get('MsgViewHelper'));
        $this->assertNull(Session::get('test'));
    }

    public function testInfo()
    {
        $this->helper->info('info message');
        $this->assertSame(array('message' => 'info message', 'type' => 'info'), Session::get('MsgViewHelper'));
        $this->assertNull(Session::get('test'));
    }

    public function testWarning()
    {
        $this->helper->warning('warning message');
        $this->assertSame(array('message' => 'warning message', 'type' => 'warning'), Session::get('MsgViewHelper'));
        $this->assertNull(Session::get('test'));
    }

    public function testToString()
    {
        $this->helper->success('yeah');
        $result = $this->helper->__toString();
        $this->assertSame('<div class="success">yeah</div>', $result);
    }

    public function testToStringEmpty()
    {
        $result = $this->helper->__toString();
        $this->assertEmpty($result);
    }

    public function testToStringWithPrefix()
    {
        $this->helper->success('yeah');
        $result = $this->helper->withPrefix('prefix')->__toString();
        $this->assertSame('<div class="prefixsuccess">yeah</div>', $result);
    }

    public function testToStringEmptyWithPrefix()
    {
        $result = $this->helper->withPrefix('prefix')->__toString();
        $this->assertEmpty($result);
    }
}
