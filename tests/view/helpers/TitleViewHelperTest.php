<?php

/*
 * @copyright NetMonsters <team@netmonsters.ru>
 * @link http://netmonsters.ru
 * @package Majestic
 * @subpackage UnitTests
 * @since 2011-10-11
 * 
 * Unit tests for PHPView class
 */

require_once dirname(__FILE__) . '/../../../Registry.php';
require_once dirname(__FILE__) . '/../../../view/helpers/ViewHelper.php';
require_once dirname(__FILE__) . '/../../../view/helpers/TitleViewHelper.php';

class TitleViewHelperTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var TitleViewHelper
     */
    public $helper;

    public function setUp()
    {
        Registry::set('TitleViewHelper', array());
        $this->helper = new TitleViewHelper('view');
    }

    public function testTitle()
    {
        $this->helper->title('one');
        $result = Registry::get('TitleViewHelper');
        $this->assertSame(array('one'), Registry::get('TitleViewHelper'));

        $this->helper->title('two');
        $this->assertSame(array('one', 'two'), Registry::get('TitleViewHelper'));
    }

    public function testSeparator()
    {
        $this->helper->title('one');
        $this->helper->title('two');

        $this->assertSame('one - two', $this->helper->__toString());
        $this->helper->setSeparator('=');
        $this->assertSame('one=two', $this->helper->__toString());
    }
}
