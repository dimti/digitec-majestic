<?php

/*
 * @copyright NetMonsters <team@netmonsters.ru>
 * @link http://netmonsters.ru
 * @package Majestic
 * @subpackage UnitTests
 * @since 2011-10-11
 * 
 * Unit tests for PHPView class
 */

require_once dirname(__FILE__) . '/../../../classes/Env.class.php';
require_once dirname(__FILE__) . '/../../../view/iView.php';
require_once dirname(__FILE__) . '/../../../view/PHPView.php';
require_once dirname(__FILE__) . '/../../../view/helpers/ViewHelper.php';
require_once dirname(__FILE__) . '/../../../view/helpers/GetViewHelper.php';

class GetViewHelperTest extends PHPUnit_Framework_TestCase
{

    /**
     * @var GetViewHelper
     */
    public $helper;

    public function setUp()
    {
        $this->helper = new GetViewHelper(new PHPView('any'));
    }

    /**
     * @TODO: GetViewHelper: initialize GetViewHelper::$get with empty array()
     */
    public function testGetWithNull()
    {
        $this->setExpectedException('PHPUnit_Framework_Error');
        $this->helper->get(null);
    }

    /**
     * @TODO: GetViewHelper: check $_GET not null
     */
    public function testGetEmptyGET()
    {
        $this->setExpectedException('PHPUnit_Framework_Error');
        $result = $this->helper->get('param');
    }

    public function testGetWithSingleParam()
    {
        $_GET['a'] = 'b';
        $result = $this->helper->get(null);
        $this->assertSame('?a=b', $result);
        $this->helper = new GetViewHelper(new PHPView('any'));
        $_GET['a'] = 'b';
        $_GET['b'] = 'a';
        $result = $this->helper->get(array('a' => 'c'));
        $this->assertSame('?a=c&amp;b=a', $result);
        $_GET['a'] = 'b';
        $_GET['b'] = 'a';
        $result = $this->helper->get(array('a'));
        $this->assertSame('?b=a', $result);
    }

    public function testGetWithArray()
    {
        $_GET['a'] = array('one' => 1, 'two' => 2);
        $_GET['b'] = 'a';
        $_GET['c'] = array('three' => 'four');
        $result = $this->helper->get(array('b' => 'c', 'c' => array('five' => 'six')));
        $this->assertSame('?a[one]=1&amp;a[two]=2&amp;b=c&amp;c[five]=six', $result);
    }

    public function testGetUrlencode()
    {
        $_GET['a'] = array('one' => 1, 'two' => 2);
        $_GET['b'] = 'a';
        $_GET['c'] = array('three' => 'four');
        $result = $this->helper->get(array('b' => 'c d e', 'c' => array('five' => 'six seven')));
        $this->assertSame('?a[one]=1&amp;a[two]=2&amp;b=c+d+e&amp;c[five]=six+seven', $result);
    }
}
