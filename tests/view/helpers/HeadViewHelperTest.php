<?php

/*
 * @copyright NetMonsters <team@netmonsters.ru>
 * @link http://netmonsters.ru
 * @package Majestic
 * @subpackage UnitTests
 * @since 2011-10-11
 * 
 * Unit tests for PHPView class
 */

require_once dirname(__FILE__) . '/../../../Registry.php';
require_once dirname(__FILE__) . '/../../../view/helpers/ViewHelper.php';
require_once dirname(__FILE__) . '/../../../view/helpers/HeadViewHelper.php';

class HeadViewHelperTest extends PHPUnit_Framework_TestCase
{

    /**
     * @var HeadViewHelper
     */
    public $helper;

    public function setUp()
    {
        Registry::set('HeadViewHelper', array());
        $this->helper = new HeadViewHelper(null);
    }

    public function testHead()
    {
        $this->helper->head('<meta />');
        $result = Registry::get('HeadViewHelper');
        $this->assertSame(array('<meta />'), Registry::get('HeadViewHelper'));

        $this->helper->head('<link />');
        $this->assertSame(array('<meta />', '<link />'), Registry::get('HeadViewHelper'));
    }

    public function testToString()
    {
        $this->helper->head('<meta />');
        $this->helper->head('<link />');

        $result = $this->helper->__toString();

        $this->assertSame("<meta />\n    <link />\n", $result);
    }
}
