<?php

/*
 * @copyright NetMonsters <team@netmonsters.ru>
 * @link http://netmonsters.ru
 * @package Majestic
 * @subpackage UnitTests
 * @since 2011-10-11
 * 
 * Unit tests for PHPView class
 */

require_once dirname(__FILE__) . '/../../../Registry.php';
require_once dirname(__FILE__) . '/../../../view/helpers/ViewHelper.php';
require_once dirname(__FILE__) . '/../../../view/helpers/BreadcrumbViewHelper.php';

class BreadcrumbViewHelperTest extends PHPUnit_Framework_TestCase
{

    /**
     * @var BreadcrumbViewHelper
     */
    public $helper;

    public function setUp()
    {
        Registry::set('BreadcrumbViewHelper', array());
        $this->helper = new BreadcrumbViewHelper(new PHPView('any'));
    }

    public function testTitle()
    {
        $this->helper->breadcrumb('Guest page', 'guest.php');
        $result = Registry::get('BreadcrumbViewHelper');
        $this->assertSame(array('Guest page' => 'guest.php'), Registry::get('BreadcrumbViewHelper'));

        $this->helper->prepend('Leave message', 'feedback.php');
        $this->assertSame(array('Leave message' => 'feedback.php', 'Guest page' => 'guest.php'), Registry::get('BreadcrumbViewHelper'));

        $this->helper->append('Home page', 'home.php');
        $this->assertSame(array('Leave message' => 'feedback.php', 'Guest page' => 'guest.php', 'Home page' => 'home.php'), Registry::get('BreadcrumbViewHelper'));
    }

    public function testToString()
    {
        $this->helper->prepend('Home page', 'home.php');
        $this->helper->breadcrumb('Guest page', 'guest.php');
        $this->helper->append('Leave message', 'feedback.php');
        $this->assertSame(array('Home page' => 'home.php', 'Guest page' => 'guest.php', 'Leave message' => 'feedback.php'), Registry::get('BreadcrumbViewHelper'));

        $result = $this->helper->__toString();
        $this->assertSame('<a href="home.php">Home page</a> &gt; <a href="guest.php">Guest page</a> &gt; <a href="feedback.php">Leave message</a>', $result);

        $this->helper->setSeparator('-');
        $result = $this->helper->__toString();
        $this->assertSame('<a href="home.php">Home page</a>-<a href="guest.php">Guest page</a>-<a href="feedback.php">Leave message</a>', $result);

        $this->helper->append('Last page', '');
        $result = $this->helper->__toString();
        $this->assertSame('<a href="home.php">Home page</a>-<a href="guest.php">Guest page</a>-<a href="feedback.php">Leave message</a>-Last page', $result);
    }
}
