<?php
/*
 * @copyright NetMonsters <team@netmonsters.ru>
 * @link http://netmonsters.ru
 * @package Majestic
 * @subpackage UnitTests
 * @since 2011-10-07
 *
 * Unit tests for Form class
 */

require_once dirname(__FILE__) . '/../../view/iView.php';
require_once dirname(__FILE__) . '/../../view/PHPView.php';
require_once dirname(__FILE__) . '/../../view/helpers/ViewHelper.php';
require_once dirname(__FILE__) . '/../../form/FormViewHelper.php';
require_once dirname(__FILE__) . '/../../session/Session.php';
require_once dirname(__FILE__) . '/../../exception/InitializationException.php';

class FormViewHelperTest extends PHPUnit_Framework_TestCase
{
    public function setUp()
    {
        $this->view = new PHPView(array('path' => '/path/to/templates'));
        $this->formname = 'formname';
        $this->test_array = array('values' => array('key1' => 'val"ue1'), 'messages' => array('field1' => 'Can\'t serialize "value"'));
        Session::set($this->formname, $this->test_array);
    }

    public function testFormWithNotViewInstance()
    {
        // @TODO check, that iView used in construct
        $form = new FormViewHelper('something');
        $this->assertInstanceOf('FormViewHelper', $form);
    }

    public function testFormUnsetFormName()
    {
        $helper = new FormViewHelper($this->view);
        $this->setExpectedException('InitializationException', 'Form name required for helper init');
        // @TODO Refactor for form name is required param?
        $helper->form();
    }

    public function testFormEmptyFormName()
    {
        $helper = new FormViewHelper($this->view);
        $this->setExpectedException('InitializationException', 'Form name required for helper init');
        $helper->form('');
    }

    public function testFillData()
    {
        $helper = new FormViewHelper($this->view);

        $this->assertAttributeSame(null, 'data', $helper);
        $return_obj = $helper->form($this->formname);
        $this->assertAttributeSame($this->test_array, 'data', $helper);
        $this->assertNull(Session::get($this->formname));
        $this->assertSame($helper, $return_obj);
    }

    public function testValueSet()
    {
        $helper = new FormViewHelper($this->view);
        $helper->form($this->formname);

        $value = $helper->value('key1');
        $this->assertSame($this->view->escape('val"ue1'), $value);
    }

    public function testValueDefault()
    {
        $helper = new FormViewHelper($this->view);
        $helper->form($this->formname);

        $value = $helper->value('key_not_exist', 'default"');
        $this->assertSame($this->view->escape('default"'), $value);
    }

    public function testMessageSet()
    {
        $helper = new FormViewHelper($this->view);
        $helper->form($this->formname);
        $value = $helper->message('field1');
        $this->assertSame('<span class="error">' . $this->view->escape('Can\'t serialize "value"') . '</span>', $value);
    }
    public function testMessageNotSet()
    {
        $helper = new FormViewHelper($this->view);
        $helper->form($this->formname);

        $value = $helper->message('key_not_exist', 'default"');
        $this->assertSame('', $value);
    }
}