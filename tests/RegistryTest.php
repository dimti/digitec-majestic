<?php

/*
 * @copyright NetMonsters <team@netmonsters.ru>
 * @link http://netmonsters.ru
 * @package Majestic
 * @subpackage UnitTests
 * @since 2011-10-06
 * 
 * Unit tests for Registry
 */

require_once dirname(__FILE__) . '/../Registry.php';

class RegistryTest extends PHPUnit_Framework_TestCase
{
    
    private $_registry = null;
    
    public function setUp()
    {
        $this->assertFalse(Registry::isRegistered(10));
        $this->_registry = Registry::getInstance();
    }
    
    public function testGetInstance()
    {
        $this->assertNotNull(Registry::getInstance());
        
        $this->assertNotNull($this->_registry);
        
        $this->assertSame(Registry::getInstance(), $this->_registry);
    }
    
    /**
     * @TODO: Registry - make __construct private
     */
//    public function testRegistryConstructor()
//    {
//        $this->setExpectedException('PHPUnit_Framework_Error'); 
//        $reg = new Registry();
//    }
    
    public function testSet()
    {
        Registry::set(1, 1);
        Registry::set('two', 2);
        $this->assertSame(Registry::get(1), $this->_registry->get(1));
        $this->assertSame(2, Registry::get('two'));
    }
    
    public function testGet()
    {
        $this->assertSame(Registry::get(1), $this->_registry->get(1));
        $this->assertSame(Registry::get('two'), 2);
        $this->assertNull(Registry::get(4));
    }
    
    /**
     * @TODO: Registry::isRegistered - check input for null
     */
    public function testIsRegistered()
    {
        $this->assertFalse(Registry::isRegistered(43));
        $this->_registry->set(3, 'three');
        $this->assertTrue(Registry::isRegistered(3));

        $this->setExpectedException('PHPUnit_Framework_Error');
        
        $this->assertFalse(Registry::isRegistered(null));
    }
}