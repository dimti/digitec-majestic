<?php

/*
 * @copyright NetMonsters <team@netmonsters.ru>
 * @link http://netmonsters.ru
 * @package Majestic
 * @subpackage UnitTests
 * @since 2011-10-11
 * 
 * Unit tests for RedisDebug class
 */

require_once dirname(__FILE__) . '/../../Registry.php';
require_once dirname(__FILE__) . '/../../Config.php';
require_once dirname(__FILE__) . '/../../exception/GeneralException.php';
require_once dirname(__FILE__) . '/../../util/profiler/CommandProfiler.php';
require_once dirname(__FILE__) . '/../../util/profiler/Profiler.php';
require_once dirname(__FILE__) . '/../../redis/RedisDebug.php';
require_once dirname(__FILE__) . '/../../exception/GeneralException.php';

class RedisDebugTest extends PHPUnit_Framework_TestCase
{

    public function run(PHPUnit_Framework_TestResult $result = NULL)
    {
        $this->setPreserveGlobalState(false);
        
        return parent::run($result);
    }

    /**
     * @group Redis
     */
    public function testConstructException()
    {
        $this->setExpectedException('GeneralException');
        $redisDebug = new RedisDebug('redis');
    }

    /**
     * @group Redis
     */
    public function testConstructGood()
    {
        $mock = $this->getMock('Redis');
        $redisDebug = new RedisDebug($mock);

        $class = new ReflectionClass('RedisDebug');
        $redis = $class->getProperty('redis');
        $redis->setAccessible(true);
        $redis = $redis->getValue($redisDebug);

        $this->assertSame($mock, $redis);
    }

    /**
     * @runInSeparateProcess
     * @group Redis
     */
    public function testCallSimpleParams()
    {
        Config::set('PROFILER', true);
        $mock = $this->getMock('Redis', array('connect'));

        $mock->expects($this->once())
                ->method('connect')
                ->with($this->equalTo('localhost'), $this->equalTo(4322))
                ->will($this->returnValue(true));

        $redisDebug = new RedisDebug($mock);
        $this->assertTrue($redisDebug->connect('localhost', 4322));
    }

    /**
     * @runInSeparateProcess
     * @group Redis
     */
    public function testCallArrayParam()
    {
        Config::set('PROFILER', true);
        $mock = $this->getMock('Redis', array('connect'));

        $mock->expects($this->once())
                ->method('connect')
                ->with($this->equalTo(array('localhost', 4322)))
                ->will($this->returnValue(true));

        $redisDebug = new RedisDebug($mock);
        $this->assertTrue($redisDebug->connect(array('localhost', 4322)));
    }

    /**
     * @runInSeparateProcess
     * @group Redis
     */
    public function testCallUndefinedMethod()
    {
        Config::set('PROFILER', true);
        $mock = $this->getMock('Redis', array('connect'));
        $redisDebug = new RedisDebug($mock);

        $this->setExpectedException('PHPUnit_Framework_Error', 'call_user_func_array() expects parameter 1 to be a valid callback, class \'Mock_Redis_');

        $this->assertNull($redisDebug->nothing('localhost', 4322));
    }
}