    <?php

/*
 * @copyright NetMonsters <team@netmonsters.ru>
 * @link http://netmonsters.ru
 * @package Majestic
 * @subpackage UnitTests
 * @since 2011-10-11
 * 
 * Unit tests for RedisManager class
 */

require_once dirname(__FILE__) . '/../../Registry.php';
require_once dirname(__FILE__) . '/../../Config.php';
require_once dirname(__FILE__) . '/../../redis/RedisManager.php';
require_once dirname(__FILE__) . '/../../exception/GeneralException.php';

class RedisManagerTest extends PHPUnit_Framework_TestCase
{
    private $connections;

    public function run(PHPUnit_Framework_TestResult $result = NULL)
    {
        $this->setPreserveGlobalState(false);
        return parent::run($result);
    }

    public function setUp()
    {
        $this->getMock('Redis');

        $conf = Config::getInstance();
        if ($conf->offsetExists('Redis')) {
            $conf->offsetUnset('Redis');
        }

        $class = new ReflectionClass('RedisManager');
        $this->connections = $class->getProperty('connections');
        $this->connections->setAccessible(true);
        $this->connections->setValue(null, array());

        set_new_overload(array($this, 'newCallback'));
    }

    /**
     * @group Redis
     */
    public function testConnectNoAnyConfig()
    {
        $this->setExpectedException('GeneralException', 'Redis config no existence');
        RedisManager::connect();
    }

    /**
     * @group Redis
     */
    public function testConnectWrongPersistantConfig()
    {
        Config::set('Redis', array('new' => 'some'));
        $this->setExpectedException('GeneralException', 'Connection parameters must be an array');
        RedisManager::connect('new');
    }

    /**
     * @group Redis
     */
    public function testConnectDefaultConfig()
    {
        Config::set('Redis', array('default' => 'some'));
        $this->setExpectedException('GeneralException', 'Connection parameters must be an array');
        RedisManager::connect();
    }

    /**
     * @group Redis
     */
    public function testConnectFailedConnection()
    {
        Config::set('Redis', array('new' => array('host' => 'error', 'port' => 2023, 'database' => 'some')));
        $this->setExpectedException('GeneralException', 'Failed to connect to Redis server at');
        RedisManager::connect('new');
    }

    /**
     * @group Redis
     */
    public function testConnectEstablishedConnectionNoDb()
    {
        Config::set('Redis', array('new' => array('host' => true, 'port' => 2023, 'database' => 'some')));
        $this->setExpectedException('GeneralException', 'Failed to select Redis database with index');
        RedisManager::connect('new');
    }

    /**
     * @group Redis
     */
    public function testConnectionGood()
    {
        Config::set('Redis', array('new' => array('host' => true, 'port' => 2023, 'database' => true)));
        $redis = RedisManager::connect('new');
        $this->assertInstanceOf('RedisMock', $redis);
    }

    /**
     * @runInSeparateProcess
     * @group Redis
     */
    public function testConnectWithDebug()
    {
        Config::set('PROFILER', true);
        Config::set('PROFILER_DETAILS', true);
        $this->getMock('RedisDebug');

        Config::set('Redis', array('new' => array('host' => true, 'port' => 2023, 'database' => true)));
        $redis = RedisManager::connect('new');
        $this->assertInstanceOf('RedisDebugMock', $redis);
    }

    /**
     * @group Redis
     */
    public function testConnectWithConfigArray()
    {
        $config = array('host' => true, 'port' => 2023, 'database' => true);
        $redis = RedisManager::connect('nsew', $config);
        $this->assertInstanceOf('RedisMock', $redis);
    }

    protected function newCallback($className)
    {
        switch ($className) {
            case 'Redis':
                return 'RedisMock';
            case 'RedisDebug':
                return 'RedisDebugMock';
            default:
                return $className;
        }
    }

    public function tearDown()
    {
        unset_new_overload();
    }
}

class RedisMock
{
    public function connect($host, $port)
    {
        if ($host === true) {
            return true;
        }
        return false;
    }

    public function select($dbname)
    {
        if ($dbname === true) {
            return true;
        }
        return false;
    }
}

class RedisDebugMock extends RedisMock
{
}