<?php
/*
 * @copyright NetMonsters <team@netmonsters.ru>
 * @link http://netmonsters.ru
 * @package Majestic
 * @subpackage UnitTests
 * @since 2011-10-06
 * 
 * Unit tests for CliLogger class
 */

require_once dirname(__FILE__) . '/../../Registry.php';
require_once dirname(__FILE__) . '/../../Config.php';
require_once dirname(__FILE__) . '/../../logger/Logger.php';
require_once dirname(__FILE__) . '/../../logger/CliLogger.php';

class CliLoggerTest extends PHPUnit_Framework_TestCase
{


    public function run(PHPUnit_Framework_TestResult $result = NULL)
    {
        $this->setPreserveGlobalState(false);
        return parent::run($result);
    }
    
    public function setUp()
    {
        Config::set('Logger', array('logger' => 'CliLogger'));
    }

    /**
     * @runInSeparateProcess
     */
    public function testGetInstance()
    {
        $logger = Logger::getInstance();
        $this->assertInstanceOf('CliLogger', $logger);
    }

    /**
     * @runInSeparateProcess
     */
    public function testLog()
    {

        Config::set('LOGGING', true);
        Config::set('Logger', array('logger' => 'CliLogger'));
        $logger = Logger::getInstance();
        ob_start();
        $logger->setPid(123);
        $logger->log('new msg');
        $out = ob_get_clean();
        $this->assertContains('<123> new msg', $out);
    }

    public function tearDown()
    {
        $conf = Config::getInstance();
        $conf->offsetUnset('Logger');
    }
}
