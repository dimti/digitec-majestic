<?php

/*
 * @copyright NetMonsters <team@netmonsters.ru>
 * @link http://netmonsters.ru
 * @package Majestic
 * @subpackage UnitTests
 * @since 2011-12-02
 *
 * Test set for MySQL
 */

require_once 'model/MySQLiDriverTest.php';
require_once 'model/MySQLiStatementTest.php';


class PackageMySQLTests
{
    public static function suite()
    {
        $suite = new PHPUnit_Framework_TestSuite('MySQL');

        $suite->addTestSuite('MySQLiDriverTest');
        $suite->addTestSuite('MySQLiStatementTest');
     
        return $suite;
    }
}