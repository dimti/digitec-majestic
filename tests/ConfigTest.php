<?php

/*
 * @copyright NetMonsters <team@netmonsters.ru>
 * @link http://netmonsters.ru
 * @package Majestic
 * @subpackage UnitTests
 * @since 2011-10-06
 * 
 * Unit tests for Config class
 */

require_once dirname(__FILE__) . '/../Registry.php';
require_once dirname(__FILE__) . '/../Config.php';
require_once dirname(__FILE__) . '/../exception/GeneralException.php';

class ConfigTest extends PHPUnit_Framework_TestCase
{

    private $_instance = null;

    public function setUp()
    {
        $this->_instance = Config::getInstance();
    }

    public function testGetInstance()
    {
        $this->assertSame($this->_instance, Config::getInstance());

        /**
         * @TODO: Config - class does not instanciate, Registry instead!!! Use late static binding
         */
        $this->assertNotEquals('Config', get_class(Config::getInstance()));
    }

    public function testArrayAsParam()
    {
        $arr = array(
            'one' => 1,
            'two' => 2,
            'three' => 3,
            4 => 'four'
        );
        Config::set(0, $arr);
        $new_arr = Config::get(0);
        $this->assertSame('ConfigArray', get_class($new_arr));
        $this->assertSame('four', $new_arr->offsetGet(4));
        $this->assertSame(1, $new_arr->one);
        $this->assertNotEquals(1, $new_arr->offsetGet('two'));
        
        $this->setExpectedException('GeneralException', 'Configuration variable');
        $new_arr->some;
    }

}