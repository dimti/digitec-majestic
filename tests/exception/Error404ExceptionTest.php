<?php

/*
 * @copyright NetMonsters <team@netmonsters.ru>
 * @link http://netmonsters.ru
 * @package Majestic
 * @subpackage UnitTests
 * @since 2011-10-11
 * 
 * Unit tests for Error404Exception class
 */

require_once dirname(__FILE__) . '/../../exception/GeneralException.php';
require_once dirname(__FILE__) . '/../../exception/ErrorHTTPException.php';
require_once dirname(__FILE__) . '/../../exception/Error404Exception.php';

class Error404ExceptionTest extends PHPUnit_Framework_TestCase
{
    public function testError404Exception()
    {
        $this->setExpectedException('Error404Exception', 404);
        throw new Error404Exception();
    }

    public function testError404ExceptionMessage()
    {
        $this->setExpectedException('Error404Exception', 'Error404Exception message', 404);
        throw new Error404Exception('Error404Exception message', 10);
    }

    public function testError404ExceptionWithPrevious()
    {
        $this->setExpectedException('Error404Exception', 'Error404Exception message', 404);
        throw new Error404Exception('Error404Exception message', 10, new ErrorException('Error message'));
    }
    
}