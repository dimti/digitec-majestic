<?php

/*
 * @copyright NetMonsters <team@netmonsters.ru>
 * @link http://netmonsters.ru
 * @package Majestic
 * @subpackage UnitTests
 * @since 2011-10-11
 * 
 * Unit tests for GeneralException class
 */

require_once dirname(__FILE__) . '/../../exception/GeneralException.php';

class GeneralExceptionTest extends PHPUnit_Framework_TestCase
{
    public function testGeneralException()
    {
        $this->setExpectedException('GeneralException');
        throw new GeneralException();
    }
    
    public function testGeneralExceptionMessage()
    {
        $this->setExpectedException('GeneralException', 'GeneralException message', 1);
        throw new GeneralException('GeneralException message', 1);
    }
    
}