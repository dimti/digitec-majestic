<?php

/*
 * @copyright NetMonsters <team@netmonsters.ru>
 * @link http://netmonsters.ru
 * @package Majestic
 * @subpackage UnitTests
 * @since 2011-10-11
 * 
 * Unit tests for ErrorHandler class
 */

require_once dirname(__FILE__) . '/../../classes/Env.class.php';
require_once dirname(__FILE__) . '/../../session/Session.php';
require_once dirname(__FILE__) . '/../../exception/ErrorHandler.php';

class ErrorHandlerTest extends PHPUnit_Framework_TestCase
{
    public $old_eh = array('PHPUnit_Util_ErrorHandler', 'handleError');
    
    public function setUp()
    {        
        set_error_handler(array('ErrorHandler', 'error_handler'));
        ob_start();
    }
    
    public function testErrorHandlerInit()
    {
        $my_eh = array('ErrorHandler', 'error_handler');
        ErrorHandler::init();
        $eh = set_error_handler($my_eh);
        $this->assertInternalType('array', $eh);
        $this->assertSame($eh, $my_eh);
    }

    public function testHandleError()
    {
        $this->setExpectedException('ErrorException', 'test error');
        trigger_error("test error", E_USER_ERROR);
    }

    public function testHandleAt()
    {
        $my_eh = array('ErrorHandler', 'error_handler');
        ErrorHandler::init();
        set_error_handler($my_eh);
        $var = '';
        $ok = @$var['some'];
        $this->assertSame('', $var);
        ob_start();
        $this->setExpectedException('ErrorException');
        $ex = $var['some'];
    }

    /**
     * @TODO: ErrorHandler->wrapTrace() not used
     */
    public function testShowDebug()
    {
        try {
            throw new ErrorException("test error", E_USER_ERROR);
        } catch (ErrorException $e) {
            $_SESSION['some'] = 'value';
            $result = ErrorHandler::showDebug($e);
            $this->assertNotEmpty($result);
            $this->assertStringStartsWith('<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">', $result);
            $this->assertStringEndsWith('</html>', $result);
        }            
    }

    /**
     * @TODO: ErrorHandler::wrapTrace not used
     * @TODO: nl2br() adds html <br /> but leaves original linebreak line \n
     */
    public function testWrapTrace()
    {
        $class = new ReflectionClass('ErrorHandler');
        $method = $class->getMethod('WrapTrace');
        $method->setAccessible(true);
        $result = $method->invoke(null, "first line\nsecond line");
        $this->assertSame("<code>first line<br />\nsecond line</code>", $result);
        $result = $method->invoke(null, "first line\r\nsecond line");
        $this->assertSame("<code>first line<br />\r\nsecond line</code>", $result);
        $result = $method->invoke(null, "first line\r\n\r\nsecond line");
        $this->assertSame("<code>first line<br />\r\n<br />\r\nsecond line</code>", $result);
    }
    
    public function tearDown()
    {
        set_error_handler($this->old_eh);
    }

}