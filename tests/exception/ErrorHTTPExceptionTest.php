<?php

/*
 * @copyright NetMonsters <team@netmonsters.ru>
 * @link http://netmonsters.ru
 * @package Majestic
 * @subpackage UnitTests
 * @since 2011-10-11
 * 
 * Unit tests for Error404Exception class
 */

require_once dirname(__FILE__) . '/../../exception/GeneralException.php';
require_once dirname(__FILE__) . '/../../exception/ErrorHTTPException.php';
require_once dirname(__FILE__) . '/../../exception/Error404Exception.php';

class ErrorHTTPExceptionTest extends PHPUnit_Framework_TestCase
{
    public function testErrorHTTPException()
    {
        $this->setExpectedException('ErrorHTTPException', 404);
        throw new ErrorHTTPException('Some error occurred', 400);
    }

    public function testErrorHTTPExceptionMessage()
    {
        $this->setExpectedException('ErrorHTTPException', 'ErrorHTTPException message', 410);
        throw new ErrorHTTPException('ErrorHTTPException message', 410);
    }

    public function testErrorHTTPExceptionWithPrevious()
    {
        $this->setExpectedException('ErrorHTTPException', 'ErrorHTTPException message', 500);
        throw new ErrorHTTPException('ErrorHTTPException message', 500, new ErrorException('Error message'));
    }

    /**
     * @dataProvider providerHTTPCode
     */
    public function testGetHTTPHeader($message, $code, $header)
    {
        try {
            throw new ErrorHTTPException($message, $code, new ErrorException('Error message'));
        }
        catch (ErrorHTTPException $e) {
            $this->assertSame($header, $e->getHTTPHeader());
            $this->assertSame($message, $e->getMessage());
            $this->assertSame($code, $e->getCode());
            $this->assertEquals(new ErrorException('Error message'), $e->getPrevious());
        }
    }

    public function providerHTTPCode()
    {
        return array(
            array('ErrorHTTPException message for 400', 400, 'HTTP/1.0 400 Bad Request'),
            array('ErrorHTTPException message for 402', 402, 'HTTP/1.0 402 Payment Required'),
            array('ErrorHTTPException message for 403', 403, 'HTTP/1.0 403 Forbidden'),
            array('ErrorHTTPException message for 404', 404, 'HTTP/1.0 404 Not Found'),
            array('ErrorHTTPException message for 410', 410, 'HTTP/1.0 410 Gone'),
            array('ErrorHTTPException message for 500', 500, 'HTTP/1.0 500 Internal Server Error'),
            array('ErrorHTTPException message for 501', 501, 'HTTP/1.0 501 Not Implemented'),
            array('ErrorHTTPException message for 503', 503, 'HTTP/1.0 503 Service Unavailable'),
        );
    }

    public function testErrorHTTPException200()
    {
        try {
            throw new ErrorHTTPException('ErrorHTTPException message', 200, new ErrorException('Error message'));
        }
        catch (ErrorHTTPException $e) {
            $this->fail();
        }
        catch (GeneralException $e) {
            $this->assertSame('Can\'t send 200 via ErrorHTTPException', $e->getMessage());
        }
    }

    public function testErrorHTTPExceptionBadCode()
    {
        try {
            throw new ErrorHTTPException('ErrorHTTPException message', 100, new ErrorException('Error message'));
        }
        catch (ErrorHTTPException $e) {
            $this->fail();
        }
        catch (GeneralException $e) {
            $this->assertSame('Incorrect HTTP code 100.', $e->getMessage());
        }
    }

    public function testErrorHTTPExceptionGoodCode()
    {
        try {
            throw new ErrorHTTPException('ErrorHTTPException message', 400, new ErrorException('Error message'));
        }
        catch (ErrorHTTPException $e) {
            $this->assertSame(400, $e->getCode());
        }
        catch (GeneralException $e) {
            $this->fail();
        }
    }
}

