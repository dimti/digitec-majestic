<?php

/*
 * @copyright NetMonsters <team@netmonsters.ru>
 * @link http://netmonsters.ru
 * @package Majestic
 * @subpackage UnitTests
 * @since 2011-11-24
 *
 * Unit tests for InitializationException class
 */

require_once dirname(__FILE__) . '/../../exception/InitializationException.php';

class InitializationExceptionTest extends PHPUnit_Framework_TestCase
{
    public function testInitializationException()
    {
        $this->setExpectedException('InitializationException');
        throw new InitializationException();
    }

    public function testInitializationExceptionMessage()
    {
        $this->setExpectedException('InitializationException', 'InitializationException message', 1);
        throw new InitializationException('InitializationException message', 1);
    }
}