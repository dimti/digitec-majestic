<?php
/**
 * Класс для работы с пользователями
 *
 * @copyright NetMonsters <team@netmonsters.ru>
 * @link
 * @package Majestic
 * @subpackage Decorator
 * @since
 */
class User
{
    static protected $user = false;

    static function login($login, $password)
    {
        if (empty($login) || empty($password)) {
            return false;
        }

        if(!preg_match(UserData::REGEXP_LOGIN, $login)) {
            return false;
        }

        self::setInfo(self::getByLogin($login));
        if (!self::getInfo()) {
            return false;
        }

        if(self::$user->pass != $password){
            return false;
        }

        self::setSession();

        return true;
    }

    static function logout()
    {
        Env::setCookie(session_name(),  '', 0);
        Env::setCookie('login',         '', 0);
        Env::setCookie('login_hash',    '', 0);
        if (session_id()) {
            session_destroy();
        }
    }

    static function process()
    {
        if (Env::getCookie(session_name())) { //есть сессия
            @session_start();
            self::setInfo(Env::Session('user'));
        } elseif (Env::getCookie('login') && Env::getCookie('login_hash')) {
            self::remember();
        }
    }

    static function setSession()
    {
        $hash = self::getHash();
        Env::setCookie('login', self::$user->login, TIME_NOW + LOGIN_COOKIE_TTL);
        Env::setCookie('login_hash', $hash, TIME_NOW + LOGIN_COOKIE_TTL);

        @session_start();

        $_SESSION['user'] = self::$user;
    }

    static function remember()
    {
        self::setInfo(self::getByLogin(Env::getCookie('login')));

        if (!self::getInfo()) {
            self::logout();
        }

        if (Env::getCookie('login_hash') == self::getHash()) {
            self::setSession();
        } else {
            self::logout();
        }
    }

    static function getHash()
    {
        return md5(self::$user->id.'hckrz'.self::$user->login.'mst'.self::$user->pass.'dai');
    }

    static function getInfo()
    {
        return Env::Session('user', self::$user);
    }

    static function setInfo($data)
    {
        self::$user = $data;
    }

    static function isGuest()
    {
        return ! (bool) Env::Session('user');
    }

    static function getByLogin($login)
    {
        $model = new UserDataModel();
        return $model->getByLogin($login);
    }

    static function getById($id)
    {
        $model = new UserDataModel();
        return $model->getById($id);
    }
}
