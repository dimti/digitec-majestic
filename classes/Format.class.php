<?php
/**
 * Format
 *
 * @copyright NetMonsters <team@netmonsters.ru>
 * @link
 * @package Majestic
 * @subpackage Core
 * @since 24.12.2008
 */

/**
 * Отвечает за конвертацию данных между читаемым
 * человеком и машиной форматами.
 *
 */
class Format
{
    /* Date & time */
    static protected $time_format        = 'H:i:s';
    static protected $date_format        = 'd.m.Y';
    static protected $today_format       = 'd.m.Y'; //disabled by default
    static protected $date_time_format   = 'H:i d.m.Y';
    static protected $today_time_format  = 'H:i d.m.Y'; //disabled by default
    static protected $timezone_offset   = 0;

    /* money */
    static protected $decimal_point   = ',';
    static protected $currency_symbol = 'руб.';
    static protected $frac_digits     = 2;

    static public function getCurrency()
    {
        return self::$currency_symbol;
    }

    public static function setCurrencySymbol($symbol)
    {
        self::$currency_symbol = $symbol;
    }

    /**
     * Форматируем int в денежный формат
     *
     * @param mixed $int
     * @param bool $currency - показывать валюту
     * @param bool $show_decimals - показывать или нет дробную часть
     * @return string
     */
    static public function int2money($int = 0, $currency = false, $show_decimals = true)
    {
        $money = number_format($int/100, ($show_decimals) ? self::$frac_digits : 0, self::$decimal_point, ' ');
        if ($currency) {
            return str_replace(' ', html_entity_decode('&nbsp;', ENT_COMPAT, 'UTF-8'), $money) . '&nbsp;' . self::$currency_symbol;
        } else {
            return $money;
        }
    }

    static public function money2int($money)
    {
        $money = str_replace(' ', '', $money);
        if(!strstr($money, self::$decimal_point) && !strstr($money, '.')){
            $int = (int)$money * 100;
        }else{
            $int = (int)str_replace(array('.', self::$decimal_point), '', $money);
        }
        return $int;
    }


    /**
     * Возвращает время в часах из секунд.
     *
     * @param int $timestamp
     * @return string
     */
    static public function int2time($timestamp = 0)
    {
         $hours     = floor($timestamp / 3600);
         $minutes   = floor(($timestamp / 60) - ($hours * 60));
         $seconds   = $timestamp - ($hours * 3600) - ($minutes * 60);

         return ($hours < 10 ? ('0' . $hours) : $hours)
            . ':' . ($minutes < 10 ? ('0' . $minutes) : $minutes)
            . ':' . ($seconds < 10 ? ('0' . $seconds) : $seconds);
    }

    /**
     * Возвращает дату и время из таймстампа.
     *
     * @param int $timestamp
     * @param bool $hours
     * @return string
     */
    static public function int2date($timestamp = 0, $hours = true)
    {
        if (date('Ymd') == date('Ymd', $timestamp)) {
            return date(($hours) ? self::$today_time_format : self::$today_format , $timestamp);
        }

        return date(($hours) ? self::$date_time_format : self::$date_format , $timestamp);
    }

    static public function int2rusDate($timestamp = 0, $hours = false)
    {
        $month = array('января', 'февраля', 'марта',
            'апреля', 'мая', 'июня',
            'июля', 'августа', 'сентября',
            'октября', 'ноября', 'декабря');
        $date = ($hours) ? date('H:i d', $timestamp) . ' ' . $month[date('m', $timestamp) - 1] . ' ' .date('Y', $timestamp) : date('d', $timestamp) . ' ' . $month[date('m', $timestamp) - 1] . ' ' .date('Y', $timestamp);
        return $date;
    }

    /**
     * Установка смещения для getTime
     *
     * @param int $offset
     */
    static public function setTimezoneOffset($offset)
    {
        self::$timezone_offset = $offset;
    }

    /**
     * Установка форматов даты
     *
     */
    static public function setDateFormat($date_time_format, $date_format = false)
    {
        self::$date_time_format = $date_time_format;
        if ($date_format) {
            self::$date_format = $date_format;
        }
    }

    /**
     * Установка форматов даты для текущего дня
     *
     */
    static public function setTodayFormat($today_time_format, $today_format = false)
    {
        self::$today_time_format = $today_time_format;
        if ($today_format) {
            self::$today_format = $today_format;
        }
    }

    /**
     * Преобразует время в секунды.
     *
     * @param string $time
     * @return int
     */
    static public function time2int($time)
    {
        $elements = explode(':', $time);
        if(count($elements) == 3){
            list($h, $m, $s) = $elements;
            $int = ($h * 60 * 60) + ($m * 60) + $s;
        } else {
            $int = (int)$time;
        }
        return $int;
    }

    /**
     * Преобразует дату в таймстамп.
     *
     * @param mixed $time
     * @return int|bool
     */
    static public function date2int($time)
    {
        return strtotime($time);
    }

    static public function int2phone($intphone)
    {
        $intphone = (string) $intphone;

        if (strlen($intphone) == 10) {
            return '(' . substr($intphone, 0, 3) . ') ' . substr($intphone, 3, 3) . '-' . substr($intphone, 6, 2) . '-' . substr($intphone, 8, 2);
        } elseif (strlen($intphone) == 7) {
            return substr($intphone, 0, 3) . '-' . substr($intphone, 3, 2) . '-' . substr($intphone, 5, 2);
        }
        return '';
    }

    static public function phone2int($phone)
    {
        $phone = str_replace(array(' ', '-', '(', ')'), '', $phone);
        $phone_length = strlen($phone);
        if (is_numeric($phone) && ($phone_length == 7 || $phone_length == 10)) { //бывают семизначные прямые номера
            if ($phone_length == 7) {
                $phone = '495' . $phone;
            }
            return $phone;
        }
        return '';
    }

    /**
     * Байты в мегабайты
     * @param int $bytes
     * @return string
     */
    public static function bytes2MB($bytes)
    {
        $mb = number_format((int) $bytes / 1024 / 1024, 2, '.', '');
        return preg_replace("/(\.?)0+$/", "", $mb) . 'МБ';
    }

    /**
     * Байты в килобайты
     * @param int $bytes
     * @return string
     */
    public static function bytes2KB($bytes)
    {
        $kb = number_format((int) $bytes / 1024, 2, '.', '');
        return preg_replace("/(\.?)0+$/", "", $kb) . 'КБ';
    }
}

/**
 * Оффсет с учетом летнего/зимнего времени
 */
Format::setTimezoneOffset(date('Z') - ((date('I') ? 60*60 : 0 )));
