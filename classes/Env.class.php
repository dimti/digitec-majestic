<?php
/**
 * Класс для работы с переменными окружения.
 *
 * @copyright NetMonsters <team@netmonsters.ru>
 * @link http://netmonsters.ru 
 * @package Majestic
 * @subpackage env
 * @since
 */

class Env
{
    
    static protected $request = array();
    
    static protected $params = array();
    
    static public function getRequestUri($trim_base = false)
    {
        if (!isset(self::$request[$trim_base])) {
            // removes get params
            list(self::$request[$trim_base], ) = explode('?', Env::Server('REQUEST_URI'));
            if ($trim_base) {
                // removes base url
                $base = FrontController::getInstance()->getBaseUrl();
                if (($length = strlen($base)) > 0 && strpos(self::$request[$trim_base], $base) === 0) {
                    self::$request[$trim_base] = (string) substr(self::$request[$trim_base], $length);
                }
            }
        }
        return self::$request[$trim_base];
    }
    
    static public function Get($key = null, $default = null)
    {
        if ($key === null) {
            return $_GET;
        }
        return (isset($_GET[$key])) ? $_GET[$key] : $default;
    }
    
    static public function Post($key = null, $default = null)
    {
        if ($key === null) {
            return $_POST;
        }
        return (isset($_POST[$key])) ? $_POST[$key] : $default;
    }
    
    static public function Cookie($key = null, $default = false)
    {
        if ($key === null) {
            return $_COOKIE;
        }
        return (isset($_COOKIE[$key])) ? $_COOKIE[$key] : $default;
    }
    
    static public function Server($key = null, $default = null)
    {
        if ($key === null) {
            return $_SERVER;
        }
        return (isset($_SERVER[$key])) ? $_SERVER[$key] : $default;
    }

    static public function setCookie($var, $value, $time = 0, $path = '/')
    {
        return setcookie($var, $value, $time, $path);
    }

    static public function Files($name = '', $default = array(), $param = false)
    {
        if (!isset($_FILES)) {
            return $default;
        }
        if (empty($name)) {
            return $_FILES;
        }
        $res = isset($_FILES[$name]) ? $_FILES[$name] : $default;
        return $param ? $res[$param] : $res;
    }
    
    static public function getParam($key = null, $default = false)
    {
        if ($key === null) {
            return self::$params;
        }
        return (isset(self::$params[$key])) ? self::$params[$key] : $default;
    }

    static public function setParam($key, $value)
    {
        self::$params[$key] = $value;
    }

    static public function setParams($params = array())
    {
        self::$params = self::$params + $params;
    }
}