<?php
/**
 * @copyright NetMonsters <team@netmonsters.ru>
 * @link http://netmonsters.ru
 * @package Majestic
 * @subpackage mail
 * @since 2010-04-25
 */

class Mailer
{
    protected $headers;
    
    public function __construct($host)
    {
        $this->headers .= 'From: noreply@' . $host . "\r\n";
        $this->headers .= 'Date: '. date('r') ."\r\n";
        $this->headers .= "Return-Path: noreply@". $host ."\r\n";
        $this->headers .= "X-Priority: 3 (Normal)\r\n";
        $this->headers .= "Message-ID: <". md5(uniqid(time()))."@". $host .">\r\n";
        $this->headers .= "MIME-Version: 1.0\r\n";
        $this->headers .= "Content-Type: text/plain; charset=utf-8\r\n";
        $this->headers .= "Content-Transfer-Encoding: 16bit\r\n";
    }
    
    public function send($email, $subject, $message)
    {
        $subject = '=?UTF-8?B?' . base64_encode($subject) . "?=\r\n";
        return mail($email, $subject, $message, $this->headers);
    }
}