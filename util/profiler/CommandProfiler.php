<?php
/**
 * @copyright NetMonsters <team@netmonsters.ru>
 * @link http://netmonsters.ru
 * @package Majestic
 * @subpackage util
 * @since 2010-03-09
 */

class CommandProfiler
{
    
    protected $type;
    protected $command = '';
    protected $start = null;
    protected $end   = null;
    
    public function __construct($type, $command)
    {
        $this->type = $type;
        $this->command = $command;
        $this->start = microtime(true);
    }
    
    public function end()
    {
        $this->end = microtime(true);
    }
    
    public function getCommand()
    {
        return $this->command;
    }

    public function getType()
    {
        return $this->type;
    }
    
    public function getElapsed()
    {
        return $this->end - $this->start;
    }
}