<?php
/**
 * @copyright NetMonsters <team@netmonsters.ru>
 * @link http://netmonsters.ru
 * @package Majestic
 * @subpackage exception
 * @since 2011-11-24
 *
 * Exception from initializtion object
 */

class InitializationException extends Exception {}