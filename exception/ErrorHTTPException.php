<?php
/**
 * @copyright NetMonsters <team@netmonsters.ru>
 * @link http://netmonsters.ru
 * @package Majestic
 * @subpackage exception
 * @since 2012-11-11
 */

class ErrorHTTPException extends GeneralException
{
    protected $http_headers; 
    
    public function __construct($message = "", $code = 0, Exception $previous = null)
    {
        $this->http_headers = array(
                    400 => 'HTTP/1.0 400 Bad Request',
                    402 => 'HTTP/1.0 402 Payment Required',
                    403 => 'HTTP/1.0 403 Forbidden',
                    404 => 'HTTP/1.0 404 Not Found',
                    410 => 'HTTP/1.0 410 Gone',
                    500 => 'HTTP/1.0 500 Internal Server Error',
                    501 => 'HTTP/1.0 501 Not Implemented',
                    503 => 'HTTP/1.0 503 Service Unavailable',
    );

        if ($code === 200) {
            throw new GeneralException('Can\'t send 200 via ErrorHTTPException', 0, $this);
        } elseif (!array_key_exists($code, $this->http_headers)) {
            throw new GeneralException('Incorrect HTTP code ' . $code . '.', 0, $this);
        }
        parent::__construct($message, $code, $previous);
    }

    public function getHTTPHeader()
    {
        return $this->http_headers[$this->code];
    }
}