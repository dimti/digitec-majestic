<?php
/**
 * @copyright NetMonsters <team@netmonsters.ru>
 * @link http://netmonsters.ru
 * @package Majestic
 * @subpackage exception
 * @since 2010-02-26
 */

class Error404Exception extends ErrorHTTPException {
    function __construct($message = '', $code = null, Exception $previous = NULL )
    {
        parent::__construct($message, 404, $previous);
    }
}