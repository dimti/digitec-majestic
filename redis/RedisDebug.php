<?php
/**
 * @copyright NetMonsters <team@netmonsters.ru>
 * @link http://netmonsters.ru
 * @package Majestic
 * @subpackage Redis
 * @since 2011-07-29
 */

class RedisDebug
{
    private $redis;

    public function __construct($redis)
    {
        if (!is_a($redis, 'Redis')) {
            throw new GeneralException();
        }
        $this->redis = $redis;
    }

    public function __call($name, $arguments)
    {
        $command = $this->r_implode(', ', $arguments);
        $profiler = Profiler::getInstance()->profilerCommand('Redis->' . $name, $command);
        $data = call_user_func_array(array($this->redis, $name), $arguments);
        $profiler->end();
        return $data;
    }

    protected function r_implode($glue, $pieces)
    {
        $retVal = array();
        foreach ($pieces as $r_pieces) {
            if (is_array($r_pieces)) {
                $retVal[] = '(' . $this->r_implode($glue, $r_pieces) . ')';
            } else {
                $retVal[] = $r_pieces;
            }
        }
        return implode($glue, $retVal);
    }
}