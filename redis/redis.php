<?php
/**
 *
 *
 * @copyright NetMonsters <team@netmonsters.ru>
 * @link http://netmonsters.ru
 * @package Majestic
 * @subpackage Redis
 * @since 2011-05-17
 */

/**
 * Redis stub for https://github.com/nicolasff/phpredis
 * @author aterekhov
 * @see https://github.com/nicolasff/phpredis
 *
 * @method bool connect() connect(string $host, int $port = 6379, float $timeout = 0) Connect to redis
 * @method bool pconnect() pconnect(string $host, int $port = 6379, float $timeout = 0, string $persistent_id) Connect to redis with reusing connection
 * @method bool select() select(int $dbindex) Switches to a given database
 * @method void close() Close connection to redis
 * @method bool setOption() setOption(mixed $name, mixed $value) Set client option
 * @method mixed getOption() getOption(mixed $name) Get client option
 * @method string ping() ping() Check the current connection status
 * @method string|bool get() get(string $key) Get the value related to the specified key
 * @method bool set() set(string $key, mixed $value, int $timeout = null) Set the string value in argument as value of the key
 * @method bool setex() setex(string $key, int $ttl, mixed $value) Set the string value in argument as value of the key, with a time to live
 * @method bool setnx() setnx(string $key, int $ttl) Set the string value in argument as value of the key if the key doesn't already exist in the database
 * @method float delete() delete(mixed $key) Remove specified keys
 * @method Redis multi() multi(mixed $mode = 1) Enter and exit transactional mode
 * @method bool exec() exec() Enter and exit transactional mode
 * @method void watch() watch(mixed $keys) Watches a key for modifications by another client
 * @method void unwatch() unwatch(mixed $keys) Watches a key for modifications by another client
 * @method void subscribe() subscribe(array $channels, mixed $callback) Subscribe to channels. Warning: this function will probably change in the future
 * @method void publish() publish(string $channel, string $message) Publish messages to channels. Warning: this function will probably change in the future
 * @method bool exists() exists(string $key) Verify if the specified key exists
 * @method int incr() incr(string $key) Increment the number stored at key by one
 * @method int incrBy() incrBy(string $key, int $value) Increment the number stored at key by $value
 *
 * @method bool expire() expire(string $key, int $ttl) Sets an expiration date (a timeout) on an item
 *
 * @method float zAdd() zAdd(string $key, float $score, string $value) Adds the specified member with a given score to the sorted set stored at key
 * @method array zRange() zRange(string $key, float $start, float $end, bool $with_scores = false) Returns a range of elements from the ordered set stored at the specified key, with values in the range [start, end]
 * @method float zDelete() zDelete(string $key, string $value) Deletes a specified member from the ordered set.
 * @method array zRevRange() zRevRange(string $key, float $start, float $end, bool $with_scores = false) Returns a range of elements from the ordered set stored at the specified key, with values in the range [start, end]
 * @method array zRangeByScore() zRangeByScore(string $key, float $start, float $end) Returns the elements of the sorted set stored at the specified key which have scores in the range [start,end]. Adding a parenthesis before start or end excludes it from the range. +inf and -inf are also valid limits. zRevRangeByScore returns the same items in reverse order, when the start and end parameters are swapped.
 * @method int zRemRangeByScore() zRemRangeByScore(string $key, float $start, float $end) Deletes the elements of the sorted set stored at the specified key which have scores in the range [start,end].
 * @method int zRemRangeByRank() zRemRangeByRank(string $key, int $start, int $end) Deletes the elements of the sorted set stored at the specified key which have rank in the range [start,end].
 * @method float zCard() zCard(string $key) Returns the cardinality of an ordered set
 * @method float zScore() zScore(string $key, string $value) Returns the score of a given member in the specified sorted set.
 * @method float zUnionStore() zUnionStore(string $destination, array $keys, array $weights = array(), string $aggregate = 'sum') Creates an union of sorted sets given in second argument and store in in first argument
 * @method float zInterStore() zInterStore(string $destination, array $keys, array $weights = array(), string $aggregate = 'sum') Creates an intersection of sorted sets given in second argument and store in in first argument
 */
class Redis
{
    
}