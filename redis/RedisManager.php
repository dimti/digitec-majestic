<?php
/**
 * @copyright NetMonsters <team@netmonsters.ru>
 * @link http://netmonsters.ru
 * @package Majestic
 * @subpackage Redis
 * @since 2010-07-17
 */

class RedisManager
{

    /**
     * Redis connections
     *
     * @var Redis[]
     */
    static protected $connections = array();

    /**
     * Connect to redis
     *
     * @param string $name connection name. If not set 'default' will be used.
     * @param array $config Configuration array.
     *
     * @throws GeneralException
     * @return Redis
     */
    static public function connect($name = 'default', $config = null)
    {
        if (!isset(self::$connections[$name])) {
            if (!$config) {
                if (!is_object(Config::get('Redis'))) {
                    throw new GeneralException('Redis config no existence');
                }
                 $config = Config::get('Redis')->$name;
            }

            if (!is_array($config)) {
                throw new GeneralException('Connection parameters must be an array');
            }

            $host = isset($config['host']) ? $config['host'] : 'localhost';
            $port = isset($config['port']) ? $config['port'] : 6379;
            $database = isset($config['database']) ? $config['database'] : 0;

            /**
             * @var Redis
             */
            $connection = new Redis();
            if (Config::get('PROFILER_DETAILS')) {
                $connection = new RedisDebug($connection);
            }
            if (!$connection->connect($host, $port)) {
                throw new GeneralException('Failed to connect to Redis server at ' . $host . ':' . $port);
            }
            if ($database) {
                if (!$connection->select($database)) {
                    throw new GeneralException('Failed to select Redis database with index ' . $database);
                }
            }
            self::$connections[$name] = $connection;
        }
        return self::$connections[$name];
    }
}