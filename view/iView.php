<?php
/**
 * @copyright NetMonsters <team@netmonsters.ru>
 * @link http://netmonsters.ru
 * @package Majestic
 * @subpackage View
 * @since 2010-02-25
 */

interface iView
{
    public function assign($name, $value = null);
    public function prepend($name, $value);
    public function append($name, $value);
    public function fetch($template);
}