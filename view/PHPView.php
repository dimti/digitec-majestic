<?php
/**
 * @copyright NetMonsters <team@netmonsters.ru>
 * @link http://netmonsters.ru
 * @package Majestic
 * @subpackage View
 * @since 2010-02-25
 */

/**
 * @method BreadcrumbViewHelper breadcrumb() breadcrumb(string $text = false, string $href = false) Append next link to breadcrumb
 * @method GetViewHelper get() get(array $replace) Replace some HTTP GET parameters with $replace
 * @method HeadViewHelper head() head(string $string = false) Append another string to HEAD section of Layout
 * @method MsgViewHelper msg() msg(string $msg = null, string $type = null) Set a message to display for user in Layout
 * @method TitleViewHelper title() title(string $string = false) Append another section for TITLE of Layout
 * @method FormViewHelper form() form(string $form = null) Get form values from session
 *
 */
class PHPView implements iView
{

    protected $path = '';

    protected $variables = array();

    protected $helpers = array();

    protected $extension = '.phtml';

    protected $template = '';

    public function __construct($config)
    {
        if (!isset($config['path'])) {
            throw new InitializationException('Configuration must have a "path" set.');
        }
        $this->setPath($config['path']);
    }

    public function getPath()
    {
        return $this->path;
    }

    public function setPath($path)
    {
        $this->path = $path;
    }

    /**
     * @param Action $object
     */
    public function assignObject($object)
    {
        foreach (get_object_vars($object) as $name => $value) {
            $this->assign($name, $value);
        }
    }

    /**
     * @param string $name
     * @param mixed $value
     */
    public function assign($name, $value = null)
    {
        $this->variables[$name] = $value;
    }

    /**
     * @param string $name
     * @param mixed $value
     */
    public function prepend($name, $value)
    {
        if (isset($this->variables[$name])) {
            $this->variables[$name] = $value . $this->variables[$name];
        } else {
            $this->variables[$name] = $value;
        }
    }

    /**
     * @param string $name
     * @param mixed $value
     */
    public function append($name, $value)
    {
        if (isset($this->variables[$name])) {
            $this->variables[$name] .= $value;
        } else {
            $this->variables[$name] = $value;
        }
    }

    public function fetch($template)
    {
        $this->template = $this->getTemplatePath($template);
        unset($template);
        extract($this->variables);
        ob_start();
        if (!is_readable($this->template)) {
            ob_clean();
            throw new GeneralException('Template "' . $this->template . '" not found.');
        }
        include($this->template);
        return ob_get_clean();
    }

    public function escape($var)
    {
        return htmlentities($var, ENT_QUOTES, 'UTF-8');
    }

    /**
     * Helpers call
     *
     * @param mixed $name
     * @param mixed $args
     * @return ViewHelper
     */
    public function __call($name, $args)
    {
        $helper = $this->getHelper($name);
        return call_user_func_array(array($helper, $name), $args);
    }

    protected function getHelper($name)
    {
        if (!isset($this->helpers[$name])) {
            $class = ucfirst($name) . 'ViewHelper';
            if (!class_exists($class)) {
                throw new GeneralException('View helper "' . $class . '" not found.');
            }
            $this->helpers[$name] = new $class($this);
        }
        return $this->helpers[$name];
    }

    protected function getTemplatePath($template)
    {
        return $this->path . $template . $this->extension;
    }
}