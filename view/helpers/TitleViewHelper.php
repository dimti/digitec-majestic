<?php
/**
 * @copyright NetMonsters <team@netmonsters.ru>
 * @link http://netmonsters.ru
 * @package Majestic
 * @subpackage View
 * @since 2010-03-16
 */

class TitleViewHelper extends ViewHelper
{

    protected $separator = ' - ';

    public function title($string = false)
    {
        if ($string) {
            $data = Registry::get(__CLASS__, array());
            $data[] = $string;
            Registry::set(__CLASS__, $data);
        }
        return $this;
    }

    public function setSeparator($sep)
    {
        $this->separator = $sep;
    }

    public function __toString()
    {
        return implode($this->separator, Registry::get(__CLASS__, array()));
    }
}