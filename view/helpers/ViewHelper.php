<?php
/**
 * @copyright NetMonsters <team@netmonsters.ru>
 * @link http://netmonsters.ru
 * @package Majestic
 * @subpackage View
 * @since 2010-03-09
 */

abstract class ViewHelper
{

    /**
     * @var PHPView
     */
    protected $view = null;

    public function __construct($view)
    {
        $this->view = $view;
    }
}