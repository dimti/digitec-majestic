<?php
/**
 * @copyright NetMonsters <team@netmonsters.ru>
 * @link http://netmonsters.ru
 * @package Majestic
 * @subpackage View
 * @since 2010-03-16
 */

class BreadcrumbViewHelper extends ViewHelper
{

    protected $separator = ' &gt; ';

    public function breadcrumb($text = false, $href = false)
    {
        if ($text) {
            $this->append($text, $href);
        }
        return $this;
    }

    public function prepend($text, $href)
    {
        Registry::set(__CLASS__, array($text => $href) + Registry::get(__CLASS__, array()));
    }

    public function append($text, $href)
    {
        Registry::set(__CLASS__, Registry::get(__CLASS__, array()) + array($text => $href));
    }

    public function setSeparator($sep)
    {
        $this->separator = $sep;
    }

    public function __toString()
    {
        $data = array();
        foreach (Registry::get(__CLASS__, array()) as $text => $href) {
            if ($href) {
                $data[] = '<a href="' . $this->view->escape($href) . '">' . $this->view->escape($text) . '</a>';
            } else {
                $data[] = $this->view->escape($text);
            }
        }
        return implode($this->separator, $data);
    }
}