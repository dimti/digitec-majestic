<?php
/**
 * @copyright NetMonsters <team@netmonsters.ru>
 * @link http://netmonsters.ru
 * @package Majestic
 * @subpackage View
 * @since 2010-03-16
 */

class HeadViewHelper extends ViewHelper
{

    public function head($string = false)
    {
        if ($string) {
            $data = Registry::get(__CLASS__, array());
            $data[] = $string;
            Registry::set(__CLASS__, $data);
        }
        return $this;
    }

    public function __toString()
    {
        return implode("\n    ", Registry::get(__CLASS__, array())) . "\n";
    }
}