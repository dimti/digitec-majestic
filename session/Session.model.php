<?php
/**
 * @copyright NetMonsters <team@netmonsters.ru>
 * @link http://netmonsters.ru
 * @package Majestic
 * @subpackage Session
 * @since 2010-02-28
 */

class SessionModel extends SqlModel
{
    
    protected $life_time;
    
    public function __construct()
    {
        parent::__construct();
        $this->life_time = get_cfg_var('session.gc_maxlifetime');
    }
    
    /* Session handler methods */
    
    public function open($save_path, $sess_name)
    {
        return true;
    }

    public function close()
    {
        return true;
    }

    public function read($id)
    {
        $sql = 'SELECT `data` FROM :table WHERE `id`=? AND `expires` > UNIX_TIMESTAMP()';
        return (string) $this->fetchField($sql, $id, 'data');
    }

    public function write($id, $data)
    {
        preg_match('/user\|.+s:2:"id";s:(\d+):"(\d+)"/', $data, $match);
        $user_id = empty($match) ? 0 : (int) $match[2];
        
        $ip = Env::Server('HTTP_X_FORWARDED_FOR', Env::Server('REMOTE_ADDR'));
        
        $update = array(
            'expires' => new DbExpr('UNIX_TIMESTAMP() + ' . (int) $this->life_time),
            'ip' => new DbExpr('INET_ATON(' . $this->quote($ip)  . ')'),
            'user_id' => $user_id, 
            'data' => $data
        );
        return (bool) $this->insert(array('id' => $id) + $update, $update);
    }

    public function destroy($id)
    {
        return (bool) $this->db->delete($this->table(), array('`id`=?' => (string) $id));
    }

    public function gc($max_life_time)
    {
        return (bool) $this->db->delete($this->table(), '`expires` < UNIX_TIMESTAMP()');
    }
    
    /* End of Session handler methods */
    
    
    public function destroyByUserId($user_id)
    {
        return $this->db->delete($this->table(), array('`user_id`=?' => $user_id));
    }
}