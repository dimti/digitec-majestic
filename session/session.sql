--
-- $Id$
--

DROP TABLE IF EXISTS `session`;

CREATE TABLE `session` (
  `id` CHAR( 32 ) NOT NULL ,
  `expires` INT UNSIGNED NOT NULL ,
  `ip` INT UNSIGNED NOT NULL ,
  `user_id` INT UNSIGNED NOT NULL DEFAULT 0,
  `data` TEXT NOT NULL ,
  PRIMARY KEY ( `id` ) ,
  INDEX `expires` ( `expires` ) ,
  INDEX `user_id` ( `user_id` )
) ENGINE = InnoDB DEFAULT CHARACTER SET = utf8;