<?php
/**
 * @copyright NetMonsters <team@netmonsters.ru>
 * @link http://netmonsters.ru
 * @package Majestic
 * @subpackage Layout
 * @since 2010-02-25
 */

abstract class Layout
{
    
    protected $template;
    
    /**
     * @var PHPView
     */
    protected $view;
    
    public function __construct()
    {
        $this->view = FrontController::getInstance()->getView();
    }
    
    /**
     * @param string $name
     * @param Action $action
     */
    protected function assign($name, $action)
    {
        $this->view->assign($name, $action->fetch());
    }
    
    
    /**
     * @param string $name
     * @param Action $action
     */
    protected function append($name, $action)
    {
        $this->view->append($name, $action->fetch());
    }
    
    
    /**
     * @param string $name
     * @param Action $action
     */
    protected function prepend($name, $action)
    {
        $this->view->prepend($name, $action->fetch());
    }
    
    abstract protected function execute();
    
    /**
     * Execute Action, insert action's result html into layout template and return Layout html
     * @param Action $action
     * @return string
     */
    public function fetch($action)
    {
        $this->view->assign('content', $action->fetch());
        $this->execute();
        return $this->view->fetch($this->getTemplate());
    }

    /**
     * Return content of template
     * @return string
     */
    protected function getTemplate()
    {
        $template = ($this->template) ? $this->template : substr(get_class($this), 0, -6/*strlen('Layout')*/);
        return '/layouts/' . $template;
    }
}