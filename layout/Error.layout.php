<?php
/**
 * @copyright NetMonsters <team@netmonsters.ru>
 * @link http://netmonsters.ru
 * @package Majestic
 * @subpackage Layout
 * @since 2010-02-25
 */

class ErrorLayout extends Layout
{
    /**
     * @var GeneralException
     */
    protected $exception;

    protected function execute()
    {
    }

    /**
     * @param Exception $exception
     */
    public function setException(Exception $exception)
    {
        $this->exception = $exception;
    }
}