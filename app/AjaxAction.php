<?php
/**
 * AjaxAction
 *
 * @copyright NetMonsters <team@netmonsters.ru>
 * @link http://netmonsters.ru
 * @package Majestic
 * @subpackage app
 * @since 2011-04-27
 */

/**
 * Base class for all ajax Actions
 */
abstract class AjaxAction extends Action
{
    /**
     * Data to output
     * @var mixed
     */
    public $data = false;

    /**
     * Use json_encode
     * @var bool
     */
    protected $json_encode = true;

    function __construct()
    {
        parent::__construct();
        $this->template = 'ajax';
    }

    function fetch()
    {
        if ($this->json_encode === true) {
            header("Content-type: application/json; charset=utf-8");
        } else {
            header("Content-type: text/html; charset=utf-8");
        }
        header("Cache-Control: no-store, no-cache, must-revalidate");
        $this->view->assign('data', $this->json_encode ? json_encode($this->data) : $this->data);
        return $this->view->fetch($this->getTemplate());
    }
}
