<?php
/**
 * @copyright NetMonsters <team@netmonsters.ru>
 * @link http://netmonsters.ru
 * @package Majestic
 * @subpackage app
 * @since 2010-02-25
 */

abstract class StaticAction extends Action
{

    protected function getTemplate()
    {
        $template = ($this->template) ? $this->template : substr(get_class($this), 0, -6 /*strlen('Action')*/);
        return '/static/' . $template;
    }

    public function fetch()
    {
        return $this->view->fetch($this->getTemplate());
    }
}