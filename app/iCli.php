<?php
/**
 * @copyright NetMonsters <team@netmonsters.ru>
 * @link http://netmonsters.ru
 * @package Majestic
 * @subpackage App
 * @since 10.07.12
 *
 */

/**
 * @desc Starter cli point need implement iCli
 * @author Aleksandr Demidov
 */
interface iCli
{
    public function run();
}