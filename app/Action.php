<?php
/**
 * @copyright NetMonsters <team@netmonsters.ru>
 * @link http://netmonsters.ru
 * @package Majestic
 * @subpackage app
 * @since 2010-02-25
 */

abstract class Action
{

    protected $template;

    /**
     * @var PHPView
     */
    protected $view;

    public function __construct()
    {
        $this->view = FrontController::getInstance()->getView();
        $this->extractParams();
        $this->execute();
    }

    protected function extractParams()
    {
        foreach (Env::getParam() as $name => $value) {
            if (is_string($name)) {
                $this->$name = $value;
            }
        }
    }

    abstract protected function execute();

    /**
     * Redirect
     *
     * @param mixed $url
     */
    protected function redirect($url = null)
    {
        header('Location: ' . (($url) ? $url : Env::getRequestUri()));
        exit();
    }

    protected function getTemplate()
    {
        $class = get_class($this);
        $template = ($this->template) ? $this->template : substr($class, 0, -6 /*strlen('Action')*/);
        $dir = array_slice(explode('/', Load::getFilePath($class)), -2, 1);
        return '/actions/' . array_pop($dir) . '/' . $template;
    }

    public function fetch()
    {
        $this->view->assignObject($this);
        return $this->view->fetch($this->getTemplate());
    }
}