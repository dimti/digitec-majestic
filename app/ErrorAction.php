<?php
/**
 * @copyright NetMonsters <team@netmonsters.ru>
 * @link http://netmonsters.ru
 * @package Majestic
 * @subpackage app
 * @since 2010-02-25
 */

class ErrorAction extends Action
{

    /**
     * @var ErrorException|ErrorHTTPException
     */
    public $exception;

    protected $ajax_error = false;

    public function __construct($exception)
    {
        $this->exception = $exception;
        parent::__construct();
    }

    protected function execute()
    {
        $this->template = 500;
        if ($this->exception instanceof Error404Exception) {
            $this->template = 404;
        } elseif ($this->exception instanceof ErrorHTTPException) {
            $this->template = 'HTTP';
        }
        $this->logError();
        $this->sendHTTPCode();
    }

    public function fetch()
    {
        if ($this->isAjaxActionError()) {
            return $this->exception->getMessage();
        }
        return parent::fetch();
    }

    protected function getTemplate()
    {
        return '/actions/' . $this->template;
    }

    protected function sendHttpCode()
    {
        switch ($this->template) {
            case 404:
            case 'HTTP':
                header($this->exception->getHTTPHeader());
                break;
            default:
                header('HTTP/1.0 500 Internal Server Error');
        }
    }

    protected function logError()
    {
        if ($this->template == 500) {
            $error = 0;
            $ex = $this->exception;
            if ($ex instanceof ErrorException) {
                $error = $ex->getSeverity();
            }

            switch ($error) {
                case E_NOTICE:
                    $error = 'Notice';
                    break;
                case E_WARNING:
                    $error = 'Warning';
                    break;
                case E_ERROR:
                    $error = 'Fatal Error';
                    break;
                default:
                    $error = 'Unknown Error';
                    break;
            }
            $message = 'PHP ' . $error . ':  ' . $ex->getMessage() . ' in ' . $ex->getFile()
                    . ' on line ' . $ex->getLine();
            error_log($message);
        }
    }

    /**
     * Check, if exception was thrown from AjaxAction Class
     * @return bool
     */
    protected function isAjaxActionError()
    {
        return $this->ajax_error;
    }

    /**
     * Set if exception was thrown from AjaxAction subclass
     */
    public function setAjaxError()
    {
        $this->ajax_error = true;
    }
}