<?php
/**
 * @copyright NetMonsters <team@netmonsters.ru>
 * @link http://netmonsters.ru
 * @package Majestic
 * @subpackage app
 * @since 2010-03-07
 */

class PagerAction extends Action
{
    public $page = 1;

    public $last_page = 1;

    protected $offset = 0;

    protected $limit;

    public function __construct($limit = 20)
    {
        $this->limit = $limit;
        parent::__construct();
    }
    
    protected function execute() {}
    
    public function setCount($count)
    {
        $this->last_page = ceil($count / $this->limit);
        if (Env::Get('p') == 'last') {
            $page = $this->last_page;
        } else {
            $page = (int) Env::Get('p');
        }
        $this->page = ($page <= $this->last_page && $page > 0) ? $page : 1;
        $this->offset = $this->limit * ($this->page - 1);
    }

    public function getOffset()
    {
        return (int) $this->offset;
    }

    public function getLimit()
    {
        return (int) $this->limit;
    }

    protected function getTemplate()
    {
        $template = ($this->template) ? $this->template : substr(get_class($this), 0, -6 /*strlen('Action')*/);
        return '/actions/' . $template;
    }
}