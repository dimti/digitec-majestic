<?php
/**
 * @copyright NetMonsters <team@netmonsters.ru>
 * @link http://netmonsters.ru
 * @package Majestic
 * @subpackage app
 * @since 2010-02-25
 */

class Router
{
    /**
     * @var Route[]
     */
    protected $routes = array();

    /**
     * @var string
     */
    protected $route_name;

    protected $default_layout = 'Default';

    protected $error_layout = 'Error';

    /**
     * @var Route
     */
    protected $route;

    public function add($name, $route, $action, $params = array(), $layout = null)
    {
        if (!$layout) {
            $layout = $this->default_layout;
        }
        $this->routes[$name] = new Route($route, $action, $params, $layout);
    }

    /**
     * @param $request
     * @return bool|Route
     */
    public function route($request)
    {
        $req = explode('/', trim($request, '/'));

        foreach ($this->routes as $name => $route) {
            if ($route->match($req)) {
                $this->route_name = $name;
                $this->route = $route;
                Env::setParams($route->getParams());
                return $this->route;
            }
        }
        return false;
    }

    /**
     * Set default layout name
     * @param string $layout
     */
    public function setDefaultLayout($layout = 'Default')
    {
        $this->default_layout = $layout;
    }

    /**
     * Sets the name of error page layout
     * @param string $layout
     */
    public function setErrorLayout($layout = 'Error')
    {
        $this->error_layout = $layout;
    }

    /**
     * Returns error layout name
     * @return string Error layout name
     */
    public function getErrorLayout()
    {
        return $this->error_layout . 'Layout';
    }

    /**
     * Return current router name
     * @return string
     */
    public function getRouteName()
    {
        return $this->route_name;
    }

    /**
     * @param null|string $name
     * @return Route
     * @throws ErrorException
     */
    public function getRoute($name = null)
    {
        if (is_null($name)) {
            return $this->route;
        } else {
            if ($this->routeIsExists($name)) {
                return $this->getRouteByName($name);
            } else {
                throw new ErrorException('Unknown route name: "' . $name . '".');
            }
        }
    }

    /**
     * @param string $name
     * @return bool
     */
    public function routeIsExists($name)
    {
        return array_key_exists($name, $this->routes);
    }

    /**
     * @param string $name
     * @return Route
     */
    protected function getRouteByName($name)
    {
        return $this->routes[$name];
    }
}