<?php
/**
 * @copyright NetMonsters <team@netmonsters.ru>
 * @link http://netmonsters.ru
 * @package Majestic
 * @subpackage Logger
 * @since 21-11-2011
 * @user: agrebnev
 */

class CliLogger extends Logger
{
    protected function concreteLog($message)
    {
        // Заменяем окончания строк на их символы
        $message = str_replace(array("\r", "\n"), array('\r', '\n'), $message);
        $out = microtime(true) . "   \t: " . $this->pid . trim($message) . PHP_EOL;
        print($out);
    }
}
 
