<?php
/**
 * @copyright NetMonsters <team@netmonsters.ru>
 * @link http://netmonsters.ru
 * @package Majestic
 * @subpackage Logger
 * @since 21-11-2011
 * @user: agrebnev
 */

abstract class Logger
{

    protected static $_instance = null;

    /**
     * pid текущего процесса
     * @var string
     */
    protected $pid = '';

    protected function __construct()
    {
    }

    /**
     * @static
     * @return Logger
     */
    public static function getInstance()
    {
        if (static::$_instance === null) {
            //$class = get_called_class();
            $class = Config::get('Logger')->logger;
            static::$_instance = new $class();
        }
        return static::$_instance;
    }

    /**
     * Вывод лога
     * @param string $message Сообщение
     */
    public function log($message)
    {
        if (Config::get('LOGGING')) {
            $this->concreteLog($message);
        }
    }

    /**
     * Установить pid текущего процесса для вывода в лог
     * @param int $pid
     */
    public function setPid($pid)
    {
        if (!empty($pid)) {
            $this->pid = ' <' . $pid . '> ';
        }
    }

    abstract protected function concreteLog($message);
}