<?php
/**
 * @copyright NetMonsters <team@netmonsters.ru>
 * @link http://netmonsters.ru
 * @package Majestic
 * @subpackage Logger
 * @since 21-11-2011
 * @user: agrebnev
 */

class FileLogger extends Logger
{

    protected $file_path = '';

    /**
     * @var resource
     */
    protected $handler = null;

    protected function __construct()
    {
        $this->file_path = Config::get('Logger')->filepath;
    }

    protected function concreteLog($message)
    {
        $out = microtime(true) . "   \t: " . $this->pid . trim($message) . "\r\n";

        if (!$this->handler) {
            $this->handler = @fopen($this->file_path, "a");
            if (!$this->handler) {
                throw new GeneralException('Could not open file ' . $this->file_path);
            }
        }
        fwrite($this->handler, $out);
    }

    public function __destruct()
    {
        if ($this->handler) {
            if (fclose($this->handler)) {
                $this->handler = null;
            }
        }
    }
}
 
