<?php
/**
 * @copyright NetMonsters <team@netmonsters.ru>
 * @link http://netmonsters.ru
 * @package Majestic
 * @subpackage form
 * @since 2010-04-24
 */

abstract class Form
{

    const SUCCESS = 'success';

    const ERROR = 'error';

    /**
     * @var FormField[]
     */
    protected $fields = array();

    /**
     * @var array
     */
    protected $messages = array(
        self::SUCCESS => 'Form data valid',
        self::ERROR => 'Form data invalid');

    protected $valid = true;

    public function __construct()
    {
        $this->init();
    }

    /**
     * @param string $name
     * @param bool|string $message
     * @return FormField
     */
    protected function addField($name, $message = false)
    {
        $this->fields[$name] = new FormField($message);
        return $this->fields[$name];
    }

    public function isValid($data)
    {
        if (!is_array($data)) {
            throw new InitializationException(__CLASS__ . '::' . __METHOD__ . ' expects an array');
        }

        foreach ($this->fields as $field_name => $field) {
            if (isset($data[$field_name])) {
                $this->valid &= $field->isValid($data[$field_name], $data);
            } else {
                $this->valid &= $field->isValid(null, $data);
            }
        }
        if (!$this->valid) {
            $this->fillHelperData();
        }
        return $this->valid;
    }

    public function getMessages()
    {
        $messages = array();
        foreach ($this->fields as $name => $field) {
            if ($mess = $field->getMessage()) {
                $messages[$name] = $mess;
            }
        }
        return $messages;
    }

    public function getValue($key)
    {
        if (isset($this->fields[$key])) {
            return $this->fields[$key]->getValue();
        }
        return false;
    }

    /**
     * @return array
     */
    public function getValues()
    {
        $values = array();
        foreach ($this->fields as $key => $field) {
            if (!$field->isIgnored()) {
                $values[$key] = $field->getValue();
            }
        }
        return $values;
    }

    /**
     * @return array
     */
    public function getSourceValues()
    {
        $values = array();
        foreach ($this->fields as $key => $field) {
            $values[$key] = $field->getSourceValue();
        }
        return $values;
    }

    /**
     * @return string
     */
    public function getMessageType()
    {
        return ($this->valid) ? self::SUCCESS : self::ERROR;
    }

    /**
     * @return string
     */
    public function getMessage()
    {
        return $this->messages[$this->getMessageType()];
    }

    /**
     * @param string $message
     * @return Form
     */
    public function setSuccessMessage($message)
    {
        $this->messages[self::SUCCESS] = (string) $message;
        return $this;
    }

    /**
     * @param string $message
     * @return Form
     */
    public function setErrorMessage($message)
    {
        $this->messages[self::ERROR] = (string) $message;
        return $this;
    }

    protected function fillHelperData()
    {
        $data['messages'] = $this->getMessages();
        $data['values'] = $this->getSourceValues();
        Session::set(get_class($this), $data);
    }

    abstract protected function init();
}