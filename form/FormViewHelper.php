<?php
/**
 * @copyright NetMonsters <team@netmonsters.ru>
 * @link http://netmonsters.ru
 * @package Majestic
 * @subpackage Form
 * @since 2010-04-25
 */

class FormViewHelper extends ViewHelper
{
    
    protected $data = null;
    
    public function form($form = null)
    {
        if ($this->data === null) {
            if ($form == null) {
                throw new InitializationException('Form name required for helper init');
            }
            $this->data = Session::get($form, array());
            Session::del($form);
        }
        return $this;
    }
    
    public function value($field, $default = '')
    {
        if (isset($this->data['values'][$field])) {
            return $this->view->escape($this->data['values'][$field]);
        }
        return $this->view->escape($default);
    }
    
    public function message($field)
    {
        if (isset($this->data['messages'][$field])) {
            return '<span class="error">' . $this->view->escape($this->data['messages'][$field]) . '</span>';
        }
        return '';
    }
}